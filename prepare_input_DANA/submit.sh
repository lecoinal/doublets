#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=20:30:00
##OAR --project f-image
#OAR --project iste-equ-ondes
#OAR -n prep_input_DANA
##OAR -p network_address NOT like 'ist-calcul6.ujf-grenoble.fr'
##OAR -p network_address='ist-calcul11.ujf-grenoble.fr'
##OAR -t besteffort

#################

set -e

source /soft/env.bash

PEXE=prepare_DANA_input_dataset.py

TMPDIR="${SHARED_SCRATCH_DIR}/$USER/oar.$OAR_JOB_ID"

echo "working into $TMPDIR"

mkdir -p $TMPDIR
cp $PEXE $TMPDIR/.
cp list_DANA_traces.txt $TMPDIR/.

cd $TMPDIR

module load python/python3.7
PYV="python3"

NET="YH"

while read line ; do

  START=$(date +%s.%N)

  STA=$(echo $line | awk '{print $1}')
  CHAN=$(echo $line | awk '{print $2}')

  $PYV $PEXE --net $NET --sta $STA --chan $CHAN > $NET.$STA.$CHAN.log

  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  echo "$NET.$STA.$CHAN : $DIFF sec"

done < list_DANA_traces.txt

sync
