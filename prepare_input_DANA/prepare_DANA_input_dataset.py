#!/usr/bin/env python
# -*- coding: utf-8 -*-

import h5py
import numpy as np
import os
import argparse

################################

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="prepare DANA input data")
    parser.add_argument("--net", default="YH", type=str, help="network code (default is YH)")
    parser.add_argument("--sta", default="DB03.00", type=str, help="station code (default is DB03.00)")
    parser.add_argument("--chan", default="Z", type=str, help="channel code (default is Z)")

    args = parser.parse_args()
    net = args.net
    sta = args.sta
    chan = args.chan

    yb = 2012
    yf = 2013
    db = 1
    df = 366

    datatype = np.int8

    idir = '/data/projects/f_image_perf/dinther/DANA/xcor/pre-pro_EQ_01-1Hz_wh_1bit_FULL/data_25.0hz_cooked/daily/YH/'
    odir = './'
    ofile = net+'.'+sta+'.'+chan+'.h5'
    
    h5fo = h5py.File(odir+'/'+ofile,'w')
    
    freq = 25
    ltf = 86400*freq
    
    for year in np.arange(yb, yf+1):
        cyear = '{:04d}'.format(year)
        for doy in np.arange(db, df+1):
            cdoy = '{:03d}'.format(doy)
            ifile = cyear+'/day_'+cdoy+'.h5'
            if ( os.path.isfile(idir+'/'+ifile) ):
                h5f = h5py.File(idir+'/'+ifile,'r')
                path_to_dset = net+'/'+sta+'/'+chan
                if ( path_to_dset in h5f):
                    dset = h5f[path_to_dset][:]
                    h5fo.create_dataset(cyear+'/'+cdoy, shape=(ltf,), dtype=datatype, data=dset[:].astype(datatype), compression='gzip', compression_opts=6)
    # shuffle filter makes no sense as dataset already 1Byte. bitshuffle (blosc) would be interesting... 
                else:
                    print(path_to_dset, ' does not exist in ', ifile)
                h5f.close()
            else:
                print(ifile, ' does not exist')
    h5fo.close()
