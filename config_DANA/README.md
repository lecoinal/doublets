Compute correlations and doublets for DANA configuration

0. Get code

```
> git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets.git
```

1. Go to config_DANA directory

```
> cd doublets/config_DANA
```

2. Compile the code with activating 1bit input CPP key

```
# ISTERRE clusters
> module load fftw/3.3.8_intel-18.0.1 hdf5/1.10.2_serial_intel-18.0.1
> make -f Makefile_ISTERRE
h5fc -cpp -Dlk_1bit   -O2   -c   -o constants.o ../constants.f90 
h5fc -cpp -Dlk_1bit   -O2   -c   -o corrmodule.o ../corrmodule.f90 
h5fc -cpp -Dlk_1bit   -O2   -c   -o doubletmodule.o ../doubletmodule.f90 
h5fc -cpp -Dlk_1bit   -O2   -c   -o inversionmodule.o ../inversionmodule.f90 
h5fc -cpp -Dlk_1bit   -O2   -o corr_doublet_inversion_bypair constants.o corrmodule.o doubletmodule.o inversionmodule.o ../corr_doublet_inversion_bypair.f90 -L/soft/x86_64/intel_18.0.1/fftw_3.3.8/lib -lfftw3  -mkl=sequential 
h5fc -cpp -Dlk_1bit   -O2   -o compute_Cminv corrmodule.o ../compute_Cminv.f90 -L/soft/x86_64/intel_18.0.1/fftw_3.3.8/lib -lfftw3  -mkl=sequential


# CIMENT clusters
> source /applis/ciment/v2/env.bash
> module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
> export INTEL_LICENSE_FILE=/opt/intel/licenses:/applis/site/licenses:/applis/ciment/v2/stow/data/intel_licences
> make
h5pfc -cpp -Dlk_1bit  -O2  -c   -o constants.o ../constants.f90 
h5pfc -cpp -Dlk_1bit  -O2  -c   -o corrmodule.o ../corrmodule.f90 -I/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/include
h5pfc -cpp -Dlk_1bit  -O2  -c   -o doubletmodule.o ../doubletmodule.f90 
h5pfc -cpp -Dlk_1bit  -O2  -c   -o inversionmodule.o ../inversionmodule.f90 
h5pfc -cpp -Dlk_1bit  -O2  -o corr_doublet_inversion_bypair constants.o corrmodule.o doubletmodule.o inversionmodule.o ../corr_doublet_inversion_bypair.f90 -L/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/lib -lfftw3  -mkl=sequential 
h5pfc -cpp -Dlk_1bit  -O2  -o compute_Cminv corrmodule.o ../compute_Cminv.f90 -L/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/lib -lfftw3  -mkl=sequential
```

This should create 2 binaries :
```
> ls corr_doublet_inversion_bypair compute_Cminv
compute_Cminv  corr_doublet_inversion_bypair
> ldd compute_Cminv
	linux-vdso.so.1 (0x00007ffcfa34c000)
	libmkl_intel_lp64.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_intel_lp64.so (0x00007f34f22ab000)
	libmkl_sequential.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_sequential.so (0x00007f34f0f9d000)
	libmkl_core.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_core.so (0x00007f34eee0a000)
	libsz.so.2 => /soft/x86_64/intel_18.0.1/szip_2.1.1/lib/libsz.so.2 (0x00007f34eebea000)
	libz.so.1 => /soft/x86_64/intel_18.0.1/zlib_1.2.11/lib/libz.so.1 (0x00007f34ee9ca000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f34ee973000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f34ee7ee000)
	libmpifort.so.12 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mpi/intel64/lib/libmpifort.so.12 (0x00007f34ee445000)
	libmpi.so.12 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mpi/intel64/lib/libmpi.so.12 (0x00007f34ed7c0000)
	librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f34ed7b6000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f34ed795000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f34ed5d4000)
	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f34ed5b8000)
	libimf.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libimf.so (0x00007f34ed02a000)
	libsvml.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libsvml.so (0x00007f34eb977000)
	libirng.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libirng.so (0x00007f34eb603000)
	libintlc.so.5 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libintlc.so.5 (0x00007f34eb396000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f34f2d9d000)
> ldd corr_doublet_inversion_bypair
	linux-vdso.so.1 (0x00007fff843e6000)
	libmkl_intel_lp64.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_intel_lp64.so (0x00007f2b3d0d0000)
	libmkl_sequential.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_sequential.so (0x00007f2b3bdc2000)
	libmkl_core.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64_lin/libmkl_core.so (0x00007f2b39c2f000)
	libsz.so.2 => /soft/x86_64/intel_18.0.1/szip_2.1.1/lib/libsz.so.2 (0x00007f2b39a0f000)
	libz.so.1 => /soft/x86_64/intel_18.0.1/zlib_1.2.11/lib/libz.so.1 (0x00007f2b397ef000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f2b39798000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f2b39613000)
	libmpifort.so.12 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mpi/intel64/lib/libmpifort.so.12 (0x00007f2b3926a000)
	libmpi.so.12 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/mpi/intel64/lib/libmpi.so.12 (0x00007f2b385e5000)
	librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007f2b385db000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f2b385ba000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f2b383f9000)
	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f2b383dd000)
	libimf.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libimf.so (0x00007f2b37e4f000)
	libsvml.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libsvml.so (0x00007f2b3679c000)
	libirng.so => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libirng.so (0x00007f2b36428000)
	libintlc.so.5 => /soft/intel-2018/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/libintlc.so.5 (0x00007f2b361bb000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f2b3dbc2000)
```

3. Edit the namelist : that is a configuration file :

Here is an example of computing doublets for one pair of station, during 2012001 - 2013365 2years period, using daily correlations with 500sec lag. These daily correlations are coming from averaged hourly correlations.

```
> cat namelist.skel
&namtim
  nn_datebeg = 2012001 ! 2013152 ! 2010084 ! 2011001 ! 2008220          ! Beginning julian day (format YYYYJJJ)
  nn_dateend = 2013365 ! 2013365 ! 2010105 ! 2012366 ! 2008239          ! Ending julian day (format YYYYJJJ)
  nn_mcorr = -1                ! if positive: to decrease correlation frequency inside the current day : 1: no decimation: keep hourly correlation
                                !                                                                         2,3,4,6,8,12: 2h-corr, 3h-corr, ..., 12h-corr
                                ! if negative: to decrease correlation frequency over several days : -1: daily corr
                                !                                                                    -15: 15days-corr (nn_datebeg and nn_dateend should be n*15 otherwise it cuts at the end)
/
&nambp
  cn_file1 = "<<FILE1>>"          ! input file names (without .h5 suffix), must be updated by submission script
  cn_file2 = "<<FILE2>>"
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for correlations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namparam
! this section should be used if your input dataset are 1D (vector)
  ln_corr = .true. ! .true. ! .false.
  cn_prefixdir = "" ! "2010" ! only of ln_corr = false : indicate the prefix dir (H5 group) where are correlation datasets
                             ! if there is no group : "" (without any space)
                             ! several group is OK : "2010/abs/005" (max 256 characters)
  nn_samprate = 25 ! Input sampling rate (Hz)  ! INTEGER
  nn_unitseg = 3600 ! length (sec) of input data for one time step computation
  nn_overlap = 3600  ! timestep duration (sec)
  nn_iter = 24 ! 1hour corr without any overlap -> 24 corr in one day
  sn_lag = 500 ! The lag (REAL) in sec (+/-) for which correlation is computed (if ln_corr=T) of for the correlation in the input file      ! REAL but maxlag = sn_lag * nn_samprate must be an integer !
  nn_nextpow2 = 0 ! 8192      ! if 0, npad=nph+maxlag=(nn_unitseg+nn_lag)*nn_samprate                 
                          ! INTEGER
                          ! but it could be more efficient to choose the next pow 2 value
                          ! FFTW is best at handling sizes of the form 2^a 3^b 5^c 7^d 11^e 13^f,
                          ! where e+f is either 0 or 1, and the other exponents are arbitrary.
  ln_writecorr = .true.         ! flag to write correlations or not
  ln_writedcorr = .false.     ! flag to write correlations as dble precision (obligatoire dans le cas ou l'on souhaite redemarrer avec ln_corr=false)
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for doublets
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namdoublet
  ln_doublets=.true.  ! Flag to compute doublets
  dist=<<DIST>>       ! interdistance in km
  Fmin=0.5 ! 0.08d0 ! 0.15d0 ! 0.033d0
  Fmax=1.0 ! 2.d0 ! 0.9d0 ! 0.125d0          ! MUST BE < Fs/2
  LWin=30.0 ! 10.0 ! 5.0 ! 10.d0 ! 30.d0          ! Window length (in seconds)
  distOverTmin=2.0 ! -17.5 ! 2.2d0    ! IF POSITIVE : velocity (in km/sec) to estimate the direct arrival travel time (should be lowered compared to reality because end of window is Tmin)
                                ! Tmin is computed as Tmin = dist / distOverTmin
                                ! IF NEGATIVE or ZERO : -Tmin in sec
  Tmax=500                      ! this value MUST be EQUAL to sn_lag parameter
  Overlap=0.d0 ! -100.0 ! 70.0 ! 0.0 ! 70.0 ! 0.0 ! 70.0 ! 80.d0 ! 60.d0       ! overlap en pourcent
  Fs=25.0      ! sampling frequency fo correlations
  ln_writedtmat = .true. ! .false.
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for inversion
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&naminversion
  ln_inversion=.true. ! .true.  ! Flag to invert doublets
  nn_Lcorr=1 ! 1000 ! 5
  nn_pondcoef=1 ! 100000 ! 1000
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for OMP threads
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namscheduling
  sn_chunksizeparam=0.05
/
```

4. If you need iworms inversion python code, make a link where you are submitting your job
```
> ln -s ../dvov_iworms.py .
```


5. Run with parameters to indicate the pair of stations and its interdistance :

```
> oarsub -S "./submit_DANA.sh 0 YH.DA01.00.Z YH.DA02.00.Z 10.3"

[ADMISSION RULE] Modify resource description with type constraints
OAR_JOB_ID=5016021
```

6. Get output :

```
>  ls -1 /bettik/$USER/00000/oar.0.YH.DA01.00.Z.YH.DA02.00.Z.5016021/
corr_YH.DA01.00.Z_YH.DA02.00.Z.h5           # correlations (over only valid dates)
dtmat_YH.DA01.00.Z_YH.DA02.00.Z.h5          # doublets measurement for each subwindow (at each valid pair of dates)
dtot_YH.DA01.00.Z_YH.DA02.00.Z.h5           # "global" doublet measurement (sort of a linear regression over dtmat) (at each valid air of dates)
dvov_YH.DA01.00.Z_YH.DA02.00.Z.h5           # dv/v after inversion (Tarantola-Valette) (at each date)
iwormsdvov_YH.DA01.00.Z_YH.DA02.00.Z.h5     # dv/v after iworms inversion (at each date)
NWiniwormsdvov_YH.DA01.00.Z_YH.DA02.00.Z.h5 # dv/v after iworms inversion (at each date), but computed with using dtmat (for all subwindows)

```

7. Results :

See https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/wikis/home




