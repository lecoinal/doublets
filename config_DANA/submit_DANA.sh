#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=00:30:00
#OAR --project f-image
##OAR --project iste-equ-ondes
#OAR -n doublet
##OAR -p network_address NOT like 'ist-calcul6.ujf-grenoble.fr'
##OAR -p network_address='ist-calcul11.ujf-grenoble.fr'
##OAR -t besteffort

#################

set -e

source /applis/ciment/v2/env.bash
#source /soft/env.bash

F90EXE=corr_doublet_inversion_bypair

FILE1=$2   # "YH.DA01.00.Z" 
FILE2=$3   # "YH.DA02.00.Z" 
DIST=$4    # 10.3 # en km

ABS=1   # apply absolute value to coherence before computing iworms weighted dvov
AUTOCORR=1   # keep autocorr (diagonal coh = 1 and diagonal dt = 0) for computing iworms dvov and weigthed dvov

RESTART_OPT=1 # 2
# 1 : restart from these already prepared input file
# 2 : restart from correlation file : in that case ln_corr MUST be false
# you also have to fill the relevant INPUTDIR here :
case "$CLUSTER_NAME" in
  luke|dahu|bigfoot)
    INPUTDIR="/bettik/lecointre/DANA/" # -> change this for your summer or bettik inputs
    #INPUTDIR="/bettik/lecointre/CID_00000/oar.0.YH.DB10.00.E.YH.DB09.00.E.7268462/" # -> change this for your summer or bettik inputs
    ;;
  ISTERRE)
#    INPUTDIR="/data/projects/f_image/maos/0_PRE_PROCESS/data_20hz/daily/run_luke2_SDS/2010/" # for RESTART_OPT=0
    INPUTDIR="/nfs_scratch/lecoinal/oar.763129/" # for RESTART_OPT=1 ou 2
    ;;
  *)
    echo "$CLUSTER_NAME not available"
    exit 0
esac

##########################################################
# try to do not change anything under this line...

getParam() {
   # Get a parameter value from namelist
   value=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//' | awk -F= '{print $2}')
   echo $value
}

#########################################################


# if not cigri, output dir is 00000
CIGRI_CAMPAIGN_ID=${CIGRI_CAMPAIGN_ID:=00000}  # substitute if not initialized

HERE=$(pwd)

# prevent segfault -> stacksize * 2
#ulimit -s 16384
ulimit -s unlimited

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)

TMPDIR="${SHARED_SCRATCH_DIR}/$USER/CID_${CIGRI_CAMPAIGN_ID}/oar.${1}.${FILE1}.${FILE2}.${OAR_JOB_ID}"
OUTDIR="${SHARED_SCRATCH_DIR}/$USER/${CIGRI_CAMPAIGN_ID}"

echo "$TMPDIR"

mkdir -p $OUTDIR
mkdir -p $TMPDIR
cp $F90EXE $TMPDIR/.
cp dvov_iworms.py $TMPDIR/.
cp compute_Cminv $TMPDIR/.

sed -e "s/<<FILE1>>/$FILE1/" -e "s/<<FILE2>>/$FILE2/" -e "s/<<DIST>>/$DIST/" namelist.skel > $TMPDIR/namelist

#############################

cd $TMPDIR

ln_inversion=$(getParam ln_inversion)

#####################################

echo "Get input:                               Date and Time: $(date +%F" "%T"."%N)"

START=$(date +%s.%N)

if [[ "$RESTART_OPT" == "1" ]] ; then
  # Option 1
  # start from input data alread "prepared"
  ln -s $INPUTDIR/${FILE1}.h5 .
  ln -sf $INPUTDIR/${FILE2}.h5 .  # force link if we want to compute autocorr
elif [[ "$RESTART_OPT" == "2" ]] ; then
  # Option 2
  echo "Restart from existing correlation file"
  # Here we give the possibility to restart from an existing correlation file
  ## get a correlation file (if ln_corr = false)
  ln -s $INPUTDIR/corr_${FILE1}_${FILE2}.h5 . 
else
  "RESTART_DIR option : $RESTART_DIR not supported"
fi

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Total elapsed time to get input: $DIFF sec"

################################################################################################

case "$CLUSTER_NAME" in
  luke|dahu|bigfoot)
    module purge
    module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
    ;;
  ISTERRE)
    module purge
    module load fftw/3.3.8_intel-18.0.1 hdf5/1.10.2_serial_intel-18.0.1 
    ;;
esac

# prepare Cminv matrix (if ln_inversion=true)

if [ "$ln_inversion" == ".true." ] ; then

  echo "Compute Cminv:                            Date and Time: $(date +%F" "%T"."%N)"

  START=$(date +%s.%N)
  ./compute_Cminv
  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  echo "Total elapsed time to compute Cminv: $DIFF sec"

fi

##############################################################################

# Correlations - doublet - inversion

echo "Correlations-doublets-inversion:                            Date and Time: $(date +%F" "%T"."%N)"

START=$(date +%s.%N)
#export OMP_NUM_THREADS=$NBCORE
./$F90EXE
#/nfs_scratch/$USER/bin/time -f "\t%M Maximum resident set size (KB)" ./$F90EXE
# valgrind --leak-check=yes ./$F90EXE
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Total elapsed time to compute corr-doublets-inv: $DIFF sec"

###########################################################################

if [ "$ln_inversion" == ".true." ] ; then

  echo "Execute dvov_iworms.py code:                            Date and Time: $(date +%F" "%T"."%N)"

  case "$CLUSTER_NAME" in
    luke|dahu|bigfoot)
      module purge
      module load python/2.7.12_gcc-4.6.2
      PYV="python2.7"
      ;;
    ISTERRE)
      module purge
      module load python/python3.7
      PYV="python3"
      ;;
  esac
  
  START=$(date +%s.%N)
  ln -s dtot_${FILE1}_${FILE2}.h5 in.h5
  ln -s dtmat_${FILE1}_${FILE2}.h5 inNWin.h5
  # ln -s corr_${FILE1}_${FILE2}.h5 corr.h5 # needed if ln_2D=0
  $PYV dvov_iworms.py --abs $ABS --autocorr $AUTOCORR
  mv out.h5 iwormsdvov_${FILE1}_${FILE2}.h5
  mv outNWin.h5 NWiniwormsdvov_${FILE1}_${FILE2}.h5
  rm in.h5 inNWin.h5
  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  echo "Total elapsed time to compute iworms dvov: $DIFF sec"

fi

#############################################################################

# Clean working directory
rm -f ${FILE1}.h5 ${FILE2}.h5 # link for input files
rm -f compute_Cminv $F90EXE dvov_iworms.py # bin & codes
rm -f Cminv_*.h5 # temporary data
rm -f namelist # config file

# Move Final outputs in unique directory (specific to this cigri campaign id)
mv $TMPDIR $OUTDIR/.

# prevent from corrupted file if not closed properly on bettik distributed file system (when data remains in cache while oar_job terminated with success)
sync

echo "End job:                                 Date and Time: $(date +%F" "%T"."%N)"
