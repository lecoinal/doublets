!#---------------------------------
!$Id: doubletmodule.f90 221 2017-07-05 12:32:01Z lecointre $
!#---------------------------------

MODULE doubletmodule

  USE, intrinsic :: iso_c_binding
  USE HDF5

#include "types.h"
  USE constants

  IMPLICIT NONE

  CONTAINS

!------------------------------------------------------------------------------
  SUBROUTINE tukeywin(res,N,alpha)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: N
    REAL(MYKIND), INTENT(in) :: alpha
    REAL(MYKIND), DIMENSION(N), INTENT(out) :: res  
    INTEGER :: ii
  !  res(:)=ZERO
    DO ii=1,floor(0.5*alpha*(N-1)+1)
      res(ii)=ZEROCINQ*(UN+COS(pi*(DEUX*(ii-1)/(alpha*(N-1))-UN)))
    ENDDO
    DO ii=ceiling(0.5*alpha*(N-1)+1),floor((N-1)*(1-0.5*alpha)+1)
      res(ii)=UN
    ENDDO
    DO ii=ceiling((N-1)*(1-0.5*alpha)+1),N
      res(ii)=ZEROCINQ*(UN+COS(pi*(DEUX*(ii-1)/(alpha*(N-1))-DEUX/alpha+UN)))
    ENDDO
  END SUBROUTINE tukeywin
  
!------------------------------------------------------------------------------
  SUBROUTINE applyTKW(N,datain,mi,ma,tkw,dataout)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: mi,ma,N
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: datain
    REAL(MYKIND), DIMENSION(ma-mi+1), INTENT(in) :: tkw
    REAL(MYKIND), DIMENSION(N), INTENT(out) :: dataout
    INTEGER :: ii,itkw
    dataout(1:mi-1)=ZERO
    itkw=0
    DO ii=mi,ma
      itkw=itkw+1
      dataout(ii)=tkw(itkw)*datain(ii)
    ENDDO
    dataout(ma+1:N)=ZERO
  END SUBROUTINE applyTKW
  
!------------------------------------------------------------------------------
  SUBROUTINE applyTKW_noinit(N,datain,mi,ma,tkw,dataout)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: mi,ma,N
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: datain
    REAL(MYKIND), DIMENSION(ma-mi+1), INTENT(in) :: tkw
    REAL(MYKIND), DIMENSION(N), INTENT(inout) :: dataout
    INTEGER :: ii,itkw
    itkw=0
    DO ii=mi,ma
      itkw=itkw+1
      dataout(ii)=tkw(itkw)*datain(ii)
    ENDDO
  END SUBROUTINE applyTKW_noinit

!------------------------------------------------------------------------------
  SUBROUTINE applyTKW_noinit_nopad(datain,LWinFs,tkw,dataout)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: LWinFs
    REAL(MYKIND), DIMENSION(LWinFs), INTENT(in) :: datain
    REAL(MYKIND), DIMENSION(LWinFs), INTENT(in) :: tkw
    REAL(MYKIND), DIMENSION(LWinFs), INTENT(out) :: dataout
    INTEGER :: itkw
    DO itkw=1,LWinFs
      dataout(itkw)=tkw(itkw)*datain(itkw)
    ENDDO
  END SUBROUTINE applyTKW_noinit_nopad
  
!------------------------------------------------------------------------------
  SUBROUTINE computeFFT(N,WindowRef,plan,mil,ndata,angle)
    IMPLICIT NONE
!!    include 'fftw3.f03'
    INTEGER, INTENT(in) :: N,ndata,mil
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: WindowRef
    INTEGER(KIND=8), INTENT(in) :: plan
    REAL(MYKIND), DIMENSION(ndata), INTENT(out) :: angle
    COMPLEX(MYKINDCPLX), DIMENSION(N/2+1) :: you
    INTEGER :: ii
#ifdef lk_float
    CALL sfftw_execute_dft_r2c(plan, WindowRef, you)
    DO ii=1,ndata 
      angle(ii)=ATAN2(aimag(you(ii-1+mil)),real(you(ii-1+mil))) ! return youtref que entre 20 et 108 : Fmin et Fmax
    ENDDO
#else
    CALL dfftw_execute_dft_r2c(plan, WindowRef, you)
    DO ii=1,ndata 
      angle(ii)=DATAN2(dimag(you(ii-1+mil)),dble(you(ii-1+mil))) ! return youtref que entre 20 et 108 : Fmin et Fmax
    ENDDO
#endif
  END SUBROUTINE computeFFT
  
!------------------------------------------------------------------------------
  SUBROUTINE measureDt(N,nl,ndata,LWORK, &
                       FreqVec,angleref,angle, &
                       ax1,ax2,sxx1,sxx2, &
                       WindowRefTot,WindowCurrent, &
                       dt, dterr, corrval)
    IMPLICIT NONE
    REAL(MYKIND), INTENT(out)   :: dt,dterr,corrval
    INTEGER,         INTENT(in) :: N,nl,ndata,LWORK
    REAL(MYKIND),                   INTENT(in) :: ax1,ax2,sxx1,sxx2 
    REAL(MYKIND), DIMENSION(nl),   INTENT(in) :: WindowRefTot
    REAL(MYKIND), DIMENSION(nl),     INTENT(in) :: WindowCurrent
    REAL(MYKIND), DIMENSION(ndata), INTENT(in) :: FreqVec
    REAL(MYKIND), DIMENSION(ndata), INTENT(in) :: angleref,angle
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: x,y
    REAL(MYKIND) :: sW
  !$omp threadprivate(x,y)
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: WORK ! working array for DGELS
  !$omp threadprivate(work) 
    INTEGER :: INFO,ii
    if (.not. allocated(work)) then 
      ALLOCATE(WORK(LWORK)) ! a ete determine une fois avec LWORK=-1
      ALLOCATE(x(ndata),y(ndata))
    endif
    CALL pearsn(N, nl,WindowRefTot,ax1,sxx1,WindowCurrent,ax2,sxx2,corrval)
    x(:)=FreqVec(:)  ! as x will be overwritten by DGELS
    ! en fait on n'aura besoin que de l'intervalle mil:mil+ndata-1 = 20:108, pas besoin de replier la 2eme moitie
    !  DO ii=1,ndata
    !    y(ii)=MOD(angleref(ii)-angle(ii),pi2)
    !  ENDDO
    DO ii=1,ndata
      y(ii)=angleref(ii)-angle(ii)
      ! Now y E [-2*pi,2*pi]
      IF (y(ii)>pi) THEN
        y(ii)=y(ii)-pi2
      ELSEIF (y(ii)<mpi) THEN
        y(ii)=y(ii)+pi2
      ENDIF 
      ! Now y E [-pi,pi]
      y(ii)=invpi2*y(ii) ! Now y is between [-0.5 0.5]
    ENDDO
    !  y(:)=angleref(:)-angle(:)
    !! Now y E [-2*pi,2*pi]
    !  where(y>pi) y=y-pi2  ! add where clauses as when using y=mod(a,b), sign(y)=sign(a), while we want y between [-pi pi]
    !  where(y<-pi) y=y+pi2
    !! Now y E [-pi,pi]
    !  y(:)=invpi2*y(:) ! /pi2  ! Now y is between [-0.5 0.5]
    !CALL DGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
#ifdef lk_float
    CALL SGELS( 'N', ndata, 1, 1, x, ndata, y, ndata, WORK, LWORK, INFO )
#else
    CALL DGELS( 'N', ndata, 1, 1, x, ndata, y, ndata, WORK, LWORK, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    dt=y(1)

    sW=ZERO
    DO ii=2,ndata
      sW=sW+y(ii)*y(ii)
    ENDDO
    dterr=SQRT(sW/(ndata-1))/x(1)

  END SUBROUTINE measureDt
  
!------------------------------------------------------------------------------
  SUBROUTINE prepare_pearsn(ntot,n,x,ax,sxx)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: n,ntot
    REAL(MYKIND), DIMENSION(n), INTENT(in) :: x
    REAL(MYKIND), INTENT(out) :: ax,sxx ! sum((x_i-x_mean)**2)
    INTEGER :: j
    ax=ZERO
    DO j=1,n
      ax=ax+x(j)
    ENDDO
    ax=ax/ntot
    sxx=ZERO
    DO j=1,n
      sxx=sxx+(x(j)-ax)**2
    ENDDO
    sxx=sxx+(ntot-n)*(ax**2)
  END SUBROUTINE prepare_pearsn
  
!------------------------------------------------------------------------------
  SUBROUTINE pearsn(n,nl,x,ax,sxx,y,ay,syy,r)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: n,nl
    REAL(MYKIND), DIMENSION(nl), INTENT(in) :: x,y
    REAL(MYKIND), INTENT(in) :: sxx,syy,ax,ay
    REAL(MYKIND), INTENT(out) :: r
    REAL(MYKIND) :: xt,yt,sxy
    INTEGER :: j
    sxy=ZERO
    do j=1,nl
      xt=x(j)-ax
      yt=y(j)-ay
      sxy=sxy+xt*yt
    enddo
    sxy=sxy+(n-nl)*ax*ay
    r=sxy/(sqrt(sxx*syy))
  END SUBROUTINE pearsn
  
!------------------------------------------------------------------------------
  SUBROUTINE lscov(N,A,B,W,dt,stdx)
  ! Here we should use lapack DGELS instead of this subroutine !!! 
    IMPLICIT NONE
    INTEGER, INTENT(in) :: N
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: A,B,W
    REAL(MYKIND), INTENT(out) :: dt,stdx
    REAL(MYKIND) :: ApdA,s,mse
    REAL(MYKIND), DIMENSION(N,N) :: dW,asaw ! ,dWasaw
    REAL(MYKIND), DIMENSION(N) :: saw
    INTEGER :: ii,jj
    ApdA=ZERO ! 0.d0
    DO ii=1,N
      ApdA=ApdA+A(ii)*W(ii)*A(ii)
    ENDDO
    s=UN/ApdA
    !  DO ii=1,N
    !    dB(ii)=W(ii)*B(ii)
    !  ENDDO
    dt=ZERO ! 0.d0
    !dt=dot_product((1.d0/ApdA)*A(:) , dB)
    DO ii=1,N
      dt=dt+A(ii)*W(ii)*B(ii)
    ENDDO
    dt=dt*s
    DO jj=1,N
      saw(jj) = s*A(jj)*W(jj)
      DO ii=1,N
        asaw(ii,jj)=A(ii)*saw(jj)    ! ici j'avais fait une erreur, j'avais calculé A(jj)*saw(ii)
      ENDDO
    ENDDO
    dW(:,:)=ZERO
    DO jj=1,N
      dW(jj,jj)=W(jj)
      dW(jj,:)=dW(jj,:)-W(jj)*asaw(jj,:)
    ENDDO
#ifdef lk_matmul
    ! with matmul : more efficient on luke4 (whereas the matrix size is 60x60 ?)
    saw=MATMUL(dW,B)
#else
    ! with BLAS DGEMM
#ifdef lk_float
    CALL SGEMM ( 'N', 'N', N, 1, N, UN, dW, N, B, N, ZERO, saw, N)              ! saw = dW * B
#else
    CALL DGEMM ( 'N', 'N', N, 1, N, UN, dW, N, B, N, ZERO, saw, N)              ! saw = dW * B
#endif
#endif
    mse=dot_product(B,saw)/(N-1)
    stdx = SQRT(s*mse)
  END SUBROUTINE lscov
  
!------------------------------------------------------------------------------
  SUBROUTINE lscov2(N,iNm1,A,B,W,rdt,rstdx,LWORK)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: N
    REAL(MYKIND), INTENT(in) :: iNm1  ! 1/(N-1)
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: A,B
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: W
    REAL(MYKIND), INTENT(out) :: rdt,rstdx
    REAL(MYKIND) :: sW
    INTEGER :: ii
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: WORK ! working array for DGELS
!$omp threadprivate(work) 
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: x,y 
!$omp threadprivate(x,y)
    INTEGER :: INFO
    INTEGER, INTENT(in) :: LWORK
    if (.not. allocated(work)) then
      ALLOCATE(WORK(LWORK)) ! a ete determine une fois avec LWORK=-1
      ALLOCATE(x(N),y(N))
    endif
    DO ii=1,N
      sW=SQRT(W(ii))
      x(ii)=sW*A(ii)
      y(ii)=sW*B(ii)
    ENDDO
! See http://www.netlib.org/lapack/lawnspdf/lawn193.pdf section 4.1.2 to compute stdx as sqrt(c_ii)
    !CALL DGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
#ifdef lk_float
    CALL SGELS( 'N', N, 1, 1, x, N, y, N, WORK, LWORK, INFO )
#else
    CALL DGELS( 'N', N, 1, 1, x, N, y, N, WORK, LWORK, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    rdt=y(1)
    sW=ZERO
    DO ii=2,N
      sW=sW+y(ii)*y(ii)
    ENDDO
    rstdx=SQRT(sW*iNm1)/x(1)
  END SUBROUTINE lscov2
  
!------------------------------------------------------------------------------
  SUBROUTINE find_min_pos(ntmp,N,jloc)
  ! Trouve l'indice de la plus petite valeur positive ou nulle d'un tableau
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: N
    INTEGER, DIMENSION(N), INTENT(IN) :: ntmp
    INTEGER, DIMENSION(N) :: localtmp
    INTEGER :: jmax,jn
    INTEGER, DIMENSION(1), INTENT(OUT) :: jloc
    jmax=MAXVAL(ntmp)
    DO jn=1,N
      IF (ntmp(jn) .LT. 0) THEN
        localtmp(jn)=jmax
      ELSE
        localtmp(jn)=ntmp(jn)
      ENDIF
    ENDDO
    jloc = MINLOC(localtmp)
  END SUBROUTINE find_min_pos
  
!------------------------------------------------------------------------------
  SUBROUTINE find_max_neg(ntmp,N,jloc)
  ! Trouve l'indice de la plus grande valeur negative ou nulle d'un tableau
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: N
    INTEGER, DIMENSION(N), INTENT(IN) :: ntmp
    INTEGER, DIMENSION(N) :: localtmp
    INTEGER :: jmin,jn
    INTEGER, DIMENSION(1), INTENT(OUT) :: jloc
    jmin=MINVAL(ntmp)
    DO jn=1,N
      IF (ntmp(jn) .GT. 0) THEN
        localtmp(jn)=jmin
      ELSE
        localtmp(jn)=ntmp(jn)
      ENDIF
    ENDDO
    jloc = MAXLOC(localtmp)
  END SUBROUTINE find_max_neg
  
!------------------------------------------------------------------------------
  SUBROUTINE complementaire(NTOTJD,NVALIDJD,A,CA)
    implicit none
    INTEGER, INTENT(in) :: NVALIDJD,NTOTJD
    INTEGER, DIMENSION(NVALIDJD), INTENT(in) :: A
    INTEGER, DIMENSION(NTOTJD-NVALIDJD), INTENT(out) :: CA ! complementaire de A
    INTEGER :: i,idxA,idxCA 
    idxA=1
    idxCA=1
    DO i=1,NTOTJD
      if (A(idxA)==i) then
        idxA=idxA+1
      else
        CA(idxCA)=i
        idxCA=idxCA+1
      endif
    ENDDO
  END SUBROUTINE complementaire
  
!------------------------------------------------------------------------------
  SUBROUTINE map_pair(nstep_ifnogap,nstep,n_valid_index,match)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nstep_ifnogap,nstep
    INTEGER, DIMENSION(nstep), INTENT(IN) :: n_valid_index
    INTEGER, DIMENSION(nstep*(nstep-1)/2), INTENT(OUT) :: match
    INTEGER, DIMENSION(2,nstep_ifnogap*(nstep_ifnogap-1)/2) :: all_pairs
    INTEGER, DIMENSION(2,nstep*(nstep-1)/2) :: real_pairs
    INTEGER, DIMENSION(1) :: minlocation
    INTEGER :: jt,it1,it2
    ! create the 2-columns array containing all the supposed theric date pairs (if no gap)
    ! 1 2
    ! 1 3
    ! 1 4
    ! 2 3
    ! 2 4
    ! 3 4 
    jt=0
    DO it1=1,nstep_ifnogap-1
      DO it2=it1+1,nstep_ifnogap
        jt=jt+1
        all_pairs(1,jt)=it1
        all_pairs(2,jt)=it2
      ENDDO
    ENDDO
    ! create the 2-columns array containing all the real date pairs
    ! 1 3
    ! 1 4
    ! 3 4     ! if step 2 is missing  
    jt=0
    DO it1=1,nstep-1
      DO it2=it1+1,nstep
        jt=jt+1
        real_pairs(1,jt)=n_valid_index(it1)
        real_pairs(2,jt)=n_valid_index(it2)
      ENDDO
    ENDDO
    ! comment on match real_pairs dans all_pairs
    ! 2
    ! 3
    ! 6
    DO jt=1,nstep*(nstep-1)/2
      minlocation = MINLOC( ABS(all_pairs(1,:)-real_pairs(1,jt)) + ABS(all_pairs(2,:)-real_pairs(2,jt)) ) ! min(sum(abs)) should be zero and should be unique
      match(jt) = minlocation(1)
    ENDDO
  END SUBROUTINE map_pair
  
END MODULE doubletmodule
