# Documentation for computing seismic velocity variations (doublets) and invert them

This documentation explains how we performed the computation of seismic velocity variations and how we invert them.

Here is the [gitlab repository associated to the DOUBLETS (Whisper and F-IMAGE) project](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets).

## 1. Download data for 2 seismic traces

```
$ iget -rv /mantis/home/lecoinal/DATA_TESTS_DOUBLETS
D- ./DATA_TESTS_DOUBLETS :
D- ./DATA_TESTS_DOUBLETS/oar.prepSHUJUAN.76736803 :
   CI.BRE.__.Z.trace.int8.h5    2614.394 MB | 6.518 sec | 16 thr | 401.090 MB/s
   CI.DLA.__.Z.trace.int8.h5    2612.931 MB | 5.788 sec | 16 thr | 451.461 MB/s
   CI.LAF.__.Z.trace.int8.h5    2674.234 MB | 6.300 sec | 16 thr | 424.484 MB/s
   CI.LGB.__.Z.trace.int8.h5    2636.603 MB | 7.389 sec | 16 thr | 356.832 MB/s
   CI.LLS.__.Z.trace.int8.h5    2672.918 MB | 8.210 sec | 16 thr | 325.570 MB/s
   namelist_doublets_iworms.       0.005 MB | 0.327 sec | 0 thr |  0.016 MB/s
   preprocess_doublets_iworm       0.005 MB | 0.063 sec | 0 thr |  0.083 MB/s
   readme                          0.003 MB | 0.043 sec | 0 thr |  0.064 MB/s
   small.CI.BRE.__.Z.trace.i     213.430 MB | 1.228 sec | 16 thr | 173.753 MB/s
   small.CI.LLS.__.Z.trace.i     240.661 MB | 0.850 sec | 16 thr | 283.275 MB/s
   submit_preprocess_doublet       0.001 MB | 0.064 sec | 0 thr |  0.020 MB/s
```


```
$ h5ls -r small.CI.BRE.__.Z.trace.int8.h5 |head -5
/                        Group
/2000                    Group
/2000/122                Dataset {1728000}
/2000/123                Dataset {1728000}
/2000/124                Dataset {1728000}
[...]
/2001                    Group
/2001/001                Dataset {1728000}
/2001/002                Dataset {1728000}
/2001/003                Dataset {1728000}
/2001/004                Dataset {1728000}
/2001/005                Dataset {1728000}
[...]
/2001/362                Dataset {1728000}
/2001/363                Dataset {1728000}
/2001/364                Dataset {1728000}
/2001/365                Dataset {1728000}
```

## 2. Get doublets code, dependancies installation, compilation and linking

### 2.a. Get doublets code

```
> git clone https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets.git
```

### 2.b. Install dependancies
For our code to work, one should install the following dependencies:

- gfortran
- hdf5
- lapack
- blas
- fftw

### 2.c. Code compilation and linking

Now, you can compile the code using the [Makefile_local](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/Makefile_local): 

```
$ make -f Makefile_local 
$ make -f Makefile_local 
h5fc -cpp -Dlk_1bit  -O2  -c   -o constants.o constants.f90 
h5fc -cpp -Dlk_1bit  -O2  -c   -o corrmodule.o corrmodule.f90 -I/usr/include -lfftw3  
h5fc -cpp -Dlk_1bit  -O2  -c   -o doubletmodule.o doubletmodule.f90 
h5fc -cpp -Dlk_1bit  -O2  -c   -o inversionmodule.o inversionmodule.f90 
h5fc -cpp -Dlk_1bit  -O2  -o corr_doublet_inversion_bypair constants.o corrmodule.o doubletmodule.o inversionmodule.o corr_doublet_inversion_bypair.f90 -I/usr/include -lfftw3  -llapack -lblas 
h5fc -cpp -Dlk_1bit  -O2  -o compute_Cminv corrmodule.o compute_Cminv.f90 -I/usr/include -lfftw3  -llapack -lblas
```

It creates 2 binaries :

- `compute_Cminv`: This program computes the inverse matrix Cminv as `Cminv(i,j) = exp( -(dist(i,j)) / (2*Lcorr) )` with Lcorr in days. Cm and Cminv are symetric band Topelitz matrix. This program stores Cminv in packed storage and normal format. Cminv [N,N] is a covariance matrix on model params built from distance between model params. It represents the temporal smoothing of the final dt/t curve.

- `corr_doublet_inversion_bypair`: This program takes 2 input files (2 seismic traces) and computes correlations, computes doublets for each pair of dates, and then invert doublets to get one value per date

Remarque: méthode iworms aussi pour l'"inversion" : code [dvov_iworms.py](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/dvov_iworms.py)

### 2.d. For GriCAD users (Université Grenoble Alpes)

An (old) installation procedure with using MODULE environment on ISTerre (ist-oar) and on GriCAD clusters is detailed here:

[Dependencies list, and installation/compilation instructions on ISTerre/GriCAD clusters (module environment)](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/config_DANA/README.md).

An installation procedure with using Guix environment on GriCAD clusters (only luke and dahu) is detailed here:

[Dependencies list, and installation/compilation instructions on GriCAD clusters (guix environment)](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/README.md).

## 3. Prepare metadata

You need the inter-station distance (in km).

Example here:

- CI.BRE.00.Z CI.DLA.00.Z 11.550312518734689

- CI.BRE.00.Z CI.LLS.00.Z 14.12678310723991

- CI.LAF.00.Z CI.LGB.00.Z 20.57029577662673


## 4. Edit the namelist

todo: detail namelist entries...

[Example namelist](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/namelist_doublets_iworms.skel)

## 5. Run the code

Once you have completed the namelist, you can use the [wrapper submit_doublets_iworms.sh](https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/doublets/-/blob/master/submit_doublets_iworms.sh) to run the MFP code. Using this submission script, you can directly change the name of the 2 input files and the corresponding interstation distance without changing the namelist (you can use namelist as a template that is automatically filled by submission script). 

```
> ./submit_doublets_iworms.sh CI.BRE.__.Z.trace.int8 CI.LLS.__.Z.trace.int8 14.12678310723991
[...]
```





