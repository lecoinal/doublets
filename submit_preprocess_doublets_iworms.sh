#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=10:30:00
#OAR --project f-image
#OAR -n doublet
##OAR -t besteffort

#################

set -e

source /applis/site/guix-start.sh
refresh_guix doublets

PEXE=preprocess_doublets_iworms.py

WDIR="${SHARED_SCRATCH_DIR}/$USER/"

# CI.BRE.__.Z.trace.int8 CI.DLA.__.Z.trace.int8 11.550312518734689
# CI.BRE.__.Z.trace.int8 CI.LLS.__.Z.trace.int8 14.12678310723991
# CI.LAF.__.Z.trace.int8 CI.LGB.__.Z.trace.int8 20.57029577662673

HERE=$(pwd)

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)

TMPDIR="$WDIR/oar.prepSHUJUAN.$OAR_JOB_ID";        # 40TB

echo "TMPDIR: $TMPDIR"

mkdir -p $TMPDIR
cp $PEXE $TMPDIR/.

#############################

cd $TMPDIR

START=$(date +%s.%N)

for f in CI.BRE.__.Z CI.LLS.__.Z CI.DLA.__.Z CI.LAF.__.Z CI.LGB.__.Z ; do 
  echo $f
  for y in $(seq 2000 2020) ; do
    echo "$f $y"
    python3 $PEXE $f $y
  done
done

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Total elapsed time to preprocess input: $DIFF sec"

#############################################################################

# prevent from corrupted file if not closed properly on bettik distributed file system (when data remains in cache while oar_job terminated with success)
sync

echo "End job:                                 Date and Time: $(date +%F" "%T"."%N)"
