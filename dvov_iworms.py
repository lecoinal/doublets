#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of doublets projects : Fortran + python codes for      #
#    computing correlations, measuring doublets, and invert it                #
#                                                                             #
#    Copyright (C) 2019 Albanne Lecointre, Pierre-Antoine Bouttier,           #
#    Violaine Louvet, Christophe Picard, Philippe Roux
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

import numpy as np
import h5py
import sys
import argparse

""" module for inverting doublet measurement between each pair of valid dates and getting Dt at each date"""


#############################################################################

ln_plt = 0

iref = 0  # assume that first valid timestep is the reference timestep, this could be parameterize...

ln_2d = 1 # sys.argv[1]

def mkplt(in1, in2, I, N, iwin):
    import matplotlib.pyplot as plt
    plt.clf()
    plt.imshow(in1)
    plt.colorbar()
    plt.title("dtot")
    #plt.show()
    plt.savefig("dtot_"+str(iwin)+".png")
    plt.clf()
    plt.imshow(in2)
    plt.colorbar()
    plt.title("coh")
    #plt.show()
    plt.savefig("coh_"+str(iwin)+".png")

def mmap_skewsym(ippvec, N, lnac):
    '''
    ippvec: packed storage input vector, without diagonal element
    we want to have diagonal elements = 0
    '''
    if lnac:
        res = np.zeros((N,N), dtype = np.float32)  # single precision real as in input file
    else:
        res = np.nan * np.zeros((N,N), dtype = np.float32)  # single precision real as in input file
    # fill the upper and lower part of skew-symmetrc matrix
    idx = 0
    for ii in np.arange(N):
        for jj in np.arange(ii+1, N):
            res[ii,jj] = ippvec[idx]
            res[jj,ii] = -ippvec[idx]
            idx += 1
    return res

def mmap_sym(ippvec, N, lnac):
    '''
    ippvec: packed storage input vector, without diagonal element
    we want to have diagonal elements = 1
    '''
    if lnac:
        res = np.ones((N,N), dtype = np.float32)  # single precision real as in input file
    else:
        res = np.nan * np.ones((N,N), dtype = np.float32)  # single precision real as in input file
    # fill the upper and lower part of skew-symmetrc matrix
    idx = 0
    for ii in np.arange(N):
        for jj in np.arange(ii+1, N):
            res[ii,jj] = ippvec[idx]
            res[jj,ii] = ippvec[idx]
            idx += 1
    return res

def compute_dvov(obs, iref):
    sumcols = np.nansum(obs, axis=0)
    res = sumcols - sumcols[iref]
    res /= np.sum(~np.isnan(sumcols)) # normalize par le nb d'éléments non NaNs
    return res

def compute_wdvov(obs, wei, iref):
    wobs = np.multiply(obs, wei) 
    sumcols = np.nansum(wobs, axis=0)
    sumwei = np.nansum(wei, axis=0)
    res = sumcols - sumcols[iref]
    res /= sumwei
    return res

def compute_pos_terms(dtot, il, N):
    res = 0.0
    for ii in np.arange(il):
        i1 = il-1-ii
        i2 = int(ii*N - (ii*(ii+1))/2.0)
        res += dtot[i1+i2]
    return res

def compute_neg_terms(dtot, il, N):
    pt = 0.0
    startindex = int(il*N - ((il+1)*il)/2.0)
    nbterms = (N-1)-il
    res = 0.0
    for ii in np.arange(nbterms):
        res += dtot[startindex]
        startindex +=1 
    return res

def get_invalid_timesteps(mm):
    "given a 2D square matrix, find the indices of columns that contains only NaNs or 0 values"
    tmp = mm
    tmp[np.isnan(tmp)] = 0
    I = np.where(~tmp.any(axis=0))[0]
    return I

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="invert doublets")

    parser.add_argument("--abs", default=0, type=int, help="flag for applying absolute value to coherence from doublet measurement for computing weighted Dt (will be an int, default is 0 = noabs)")
    parser.add_argument("--autocorr", default=0, type=int, help="flag for keeping autocorr from doublet measurement : keeping autocorr means that coh(ti,ti)=1 and dtot(ti,ti)=0, noautocorr means that both are NaNs values (will be an int, default is 0 = noautocorr)")

    args = parser.parse_args()

    ln_abscoh = args.abs # apply absolute value to coherence before computing weighted Dvov

    ln_autocorr = args.autocorr  # autocorr (diagonal terms (1 for coh and 0 for dt) are taken into account)
    

    # dvov from dtot :

    h5f = h5py.File("in.h5", "r")
    h5fo = h5py.File("out.h5", "w")

    dtot = h5f['/dtot'][0,:]
    # dtoterr = h5f['/dtot'][1,:] # not used
    if ln_abscoh:
        coh = np.abs(h5f['/dtot'][2,:])
    else:
        coh = h5f['/dtot'][2,:]

    # get the number of timesteps
    N = int(np.ceil(np.sqrt(2.0 * np.shape(dtot)[0])))

    if ln_2d:
    # 1. Method with building 2D matrix

        # map these packed storage skew-symmetric (for dtot) and symmetric (for coh) vectors into 2D matrix
        dtot_2d = mmap_skewsym(dtot, N, ln_autocorr)
        coh_2d = mmap_sym(coh, N, ln_autocorr)
    
        # get index for invalid timesteps
        # give a list of columns with only 0 or NaN values
        I = get_invalid_timesteps(dtot_2d)
        #print(I)
        Isave = I
    
        if ln_plt:
            mkplt(dtot_2d, coh_2d, I, N, 9999)
    
    
        # remove invalid timestep from 2D matrix
        dtot_2d = np.delete(dtot_2d, I, axis=0)
        dtot_2d = np.delete(dtot_2d, I, axis=1)
        # is it always anti-symmetric ?
        #print((dtot_2d.transpose() == -dtot_2d).all())
        #print(np.shape(dtot_2d))
        coh_2d = np.delete(coh_2d, I, axis=0)
        coh_2d = np.delete(coh_2d, I, axis=1)
        # is it always symmetric ?
        #print((coh_2d.transpose() == coh_2d).all())
        #print(np.shape(coh_2d))
    
        dvov = compute_dvov(dtot_2d, iref)
        wdvov = compute_wdvov(dtot_2d, coh_2d, iref)
    
        for idx in I:
            dvov = np.insert(dvov, idx, np.nan) 
            wdvov = np.insert(wdvov, idx, np.nan) 
    else:
    # 2. Method with saving mem
       print("ln_2d=0 is not working for the moment")
       sys.exit(1)

       dvov = np.nan * np.ones((N), dtype = np.float32)  # single precision real as in input file
       wdvov = np.nan * np.ones((N), dtype = np.float32)  # single precision real as in input file


       # we need to get iarrvalid from correlation file 
       h5fi = h5py.File('corr.h5','r')
       I = h5fi['/iarrvalid'][:]
       #print(np.shape(I))
       h5fi.close()
               
       #wdtot = np.multiply(dtot, np.abs(coh))
       wdtot = np.multiply(dtot, coh)

       inorm = 0 
       for il in np.arange(N):

           if I[il]:

               inorm += 1

               pos_terms = compute_pos_terms(dtot, il, N)
               neg_terms = compute_neg_terms(dtot, il, N)
               dvov[il] = pos_terms - neg_terms

               pos_terms = compute_pos_terms(wdtot, il, N)
               neg_terms = compute_neg_terms(wdtot, il, N)
               wdvov[il] = pos_terms - neg_terms
       
       ref = dvov[iref]
       dvov[:] = dvov[:] - ref
       dvov[:] /= inorm  

       ref = wdvov[iref]
       wdvov[:] = wdvov[:] - ref
       wdvov[:] /= inorm  

    h5fo.create_dataset("dvov", (N,), shuffle="true", compression="gzip", compression_opts=6, data=dvov)
    h5fo.create_dataset("wdvov", (N,), shuffle="true", compression="gzip", compression_opts=6, data=wdvov)

    h5f.close()
    h5fo.close()

    # dvov from dtmat :

    h5f = h5py.File("inNWin.h5", "r")
    h5fo = h5py.File("outNWin.h5", "w")

    Win = h5f['/dtmat4'][:]
    NWin = np.shape(Win)[0]
        
    dset_dvov = h5fo.create_dataset("NWindvov", (NWin, N), shuffle="true", compression="gzip", compression_opts=6)
    dset_wdvov = h5fo.create_dataset("NWinwdvov", (NWin, N), shuffle="true", compression="gzip", compression_opts=6)

    for iwin in np.arange(NWin): 

        dtot = h5f['/dtmat_save'][:,0,iwin]
        # dtoterr = h5f['/dtmat_save'][:,1,iwin] # not used
        if ln_abscoh:
            coh = np.abs(h5f['/dtmat_save'][:,2,iwin])
        else:
            coh = h5f['/dtmat_save'][:,2,iwin]
    
        # get the number of timesteps: here we already have list of timesteps without missing timesteps
        N = int(np.ceil(np.sqrt(2.0 * np.shape(dtot)[0])))
    
        if ln_2d:
        # 1. Method with building 2D matrix
    
            # map these packed storage skew-symmetric (for dtot) and symmetric (for coh) vectors into 2D matrix
            dtot_2d = mmap_skewsym(dtot, N, ln_autocorr)
            coh_2d = mmap_sym(coh, N, ln_autocorr)
        
            # get index for invalid timesteps
            # give a list of columns with only 0 values
            I = np.where(~dtot_2d.any(axis=0))[0]
            #print("iwin, I", iwin, I)  # I should be empty...
        
            if ln_plt:
                mkplt(dtot_2d, coh_2d, I, N, iwin)
        
        
            # remove invalid timestep from 2D matrix
            dtot_2d = np.delete(dtot_2d, I, axis=0)
            dtot_2d = np.delete(dtot_2d, I, axis=1)
            # is it always anti-symmetric ?
            #print((dtot_2d.transpose() == -dtot_2d).all())
            #print(np.shape(dtot_2d))
            coh_2d = np.delete(coh_2d, I, axis=0)
            coh_2d = np.delete(coh_2d, I, axis=1)
            # is it always symmetric ?
            #print((coh_2d.transpose() == coh_2d).all())
            #print(np.shape(coh_2d))
        
            dvov = compute_dvov(dtot_2d, iref)
            wdvov = compute_wdvov(dtot_2d, coh_2d, iref)
        
            for idx in Isave:
                dvov = np.insert(dvov, idx, np.nan) 
                wdvov = np.insert(wdvov, idx, np.nan) 
        else:
           print("ln_2d=0 is not working for the moment")
           sys.exit(1)
        # 2. Method with saving mem
    
           dvov = np.nan * np.ones((N), dtype = np.float32)  # single precision real as in input file
           wdvov = np.nan * np.ones((N), dtype = np.float32)  # single precision real as in input file
    
    
           # we need to get iarrvalid from correlation file 
           h5fi = h5py.File('corr.h5','r')
           I = h5fi['/iarrvalid'][:]
           #print(np.shape(I))
           h5fi.close()
                   
           wdtot = np.multiply(dtot, coh)
    
           inorm = 0 
           for il in np.arange(N):
    
               if I[il]:
    
                   inorm += 1
    
                   pos_terms = compute_pos_terms(dtot, il, N)
                   neg_terms = compute_neg_terms(dtot, il, N)
                   dvov[il] = pos_terms - neg_terms
    
                   pos_terms = compute_pos_terms(wdtot, il, N)
                   neg_terms = compute_neg_terms(wdtot, il, N)
                   wdvov[il] = pos_terms - neg_terms
           
           ref = dvov[iref]
           dvov[:] = dvov[:] - ref
           dvov[:] /= inorm  
    
           ref = wdvov[iref]
           wdvov[:] = wdvov[:] - ref
           wdvov[:] /= inorm  
    
        dset_dvov[iwin,:] = dvov[:]
        dset_wdvov[iwin,:] = wdvov[:]
    
    h5f.close()
    h5fo.close()
