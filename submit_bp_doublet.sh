#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=1:00:00
#OAR --project whisper
#OAR -n doublet
#OAR -p network_address='luke24'

# we suppose the code has been compiled on luke frontend with:
# $ source /applis/ciment/v2/env.bash
# $ module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1
# $ make
# h5pfc -cpp -Dlk_1bit  -O2  -c   -o constants.o constants.f90 -I/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/include
# h5pfc -cpp -Dlk_1bit  -O2  -c   -o corrmodule.o corrmodule.f90 -I/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/include
# h5pfc -cpp -Dlk_1bit  -O2  -c   -o doubletmodule.o doubletmodule.f90 -I/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/include
# h5pfc -cpp -Dlk_1bit  -O2  -o corr_doublet_inversion_bypair constants.o corrmodule.o doubletmodule.o corr_doublet_inversion_bypair.f90 -I/applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/include /applis/ciment/v2/stow/x86_64/intel_13.0.1/fftw_3.3.4-precise/lib/libfftw3f.a /applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib/libgfortran.so.3 -L/applis/ciment/v2/stow/x86_64/gcc_4.6.2/lapack_3.3.0/ -llapack -lblas
# ifort: warning #10145: no action performed for file '/applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib/libgfortran.so.3'
# $

set -e
source /applis/ciment/v2/env.bash

F90EXE=corr_doublet_inversion_bypair

IDIR="${SHARED_SCRATCH_DIR}/$USER/"

#FILE1="AHIH_BAE"
#FILE2="ARKH_BAN"
#DIST=19.419384
FILE1="GKSH_SHE"
FILE2="MRTH_SHZ"
DIST=18.232280

#########################################################

HERE=$(pwd)

export LD_LIBRARY_PATH=/applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib:$LD_LIBRARY_PATH

cat $OAR_NODE_FILE
NBCORE=$(cat $OAR_NODE_FILE | wc -l)

case "$CLUSTER_NAME" in
  luke)
    TMPDIR="$IDIR/oar.QINGYU.$OAR_JOB_ID";        # 40TB
    #TMPDIR="$LOCAL_SCRATCH_DIR/$USER/oar.IDRISdoublet.BP.$OAR_JOB_ID";        # 155GB
    ;;
esac

mkdir -p $TMPDIR
cp $F90EXE $TMPDIR/.
cp compute_Cminv $TMPDIR/.
sed -e "s/<<FILE1>>/$FILE1/" -e "s/<<FILE2>>/$FILE2/" -e "s/<<DIST>>/$DIST/" namelist.skel > $TMPDIR/namelist

cd $TMPDIR

echo "Get input:                               Date and Time: $(date +%F" "%T"."%N)"
#ln -sf ${SHARED_SCRATCH_DIR}/lecointre/JAPAN_BYPAIR/0.02_0.125/${TR1}_${C1}.h5 .
#ln -sf ${SHARED_SCRATCH_DIR}/lecointre/JAPAN_BYPAIR/0.02_0.125/${TR2}_${C2}.h5 .
#ln -sf /scratch_md1200/lecointre/JAPAN_BYPAIR/0.08_2/${TR1}_${C1}.h5 .
#ln -sf /scratch_md1200/lecointre/JAPAN_BYPAIR/0.08_2/${TR2}_${C2}.h5 .
ln -sf ${SHARED_SCRATCH_DIR}/lecointre/JAPAN_BYPAIR/0.08_2/${FILE1}.h5 .
ln -sf ${SHARED_SCRATCH_DIR}/lecointre/JAPAN_BYPAIR/0.08_2/${FILE2}.h5 .

echo "Compute Cminv:                            Date and Time: $(date +%F" "%T"."%N)"

./compute_Cminv

#ln -sf Cminv_N00000020_L00000005_P00100000.h5 Cminv.h5

# Correlations

echo "Execute $F90EXE code:                            Date and Time: $(date +%F" "%T"."%N)"

#export OMP_NUM_THREADS=$NBCORE
./$F90EXE
#/nfs_scratch/$USER/bin/time -f "\t%M Maximum resident set size (KB)" ./$F90EXE
# valgrind --leak-check=yes ./$F90EXE

#echo "Move outputs:                            Date and Time: $(date +%F" "%T"."%N)"

# rm ${FILE1}.h5 ${FILE2}.h5
# mv $TMPDIR $IDIR/.

echo "End job:                                 Date and Time: $(date +%F" "%T"."%N)"
