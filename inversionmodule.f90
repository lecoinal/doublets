!#---------------------------------
!$Id: inversionmodule.f90 225 2017-07-06 07:43:07Z lecointre $
!#---------------------------------

MODULE inversionmodule

  USE, intrinsic :: iso_c_binding

#include "types.h"

  IMPLICIT NONE

  CONTAINS

!------------------------------------------------------------------------------
  FUNCTION Compute_Gmatij(ii,jj,Nd) RESULT(Gmatij)
    IMPLICIT NONE
    INTEGER :: ii,jj,Nd
    INTEGER :: Gmatij
    INTEGER :: ik
    INTEGER, DIMENSION(2) :: iiloc
    ik=0
    iiloc(2)=ii
    DO WHILE ( iiloc(2) > 0 )
      ik=ik+1
      iiloc(1)=iiloc(2)
      iiloc(2)=iiloc(2)-(Nd-ik)
    ENDDO
    IF ( jj==ik ) THEN
      Gmatij=-1 ! .d0
    ELSEIF ( jj > ik ) THEN
      IF ( jj == iiloc(1)+ik ) THEN
        Gmatij=1 ! .d0
      ELSE
        Gmatij=0 ! .d0
      ENDIF
    ELSE
      Gmatij=0 ! .d0
    ENDIF
    RETURN
  END FUNCTION Compute_Gmatij
! Calling this function:
! INTEGER :: ii,jj,Ndays
! Gmatij=Compute_Gmatij(ii,jj,Ndays)
!              <====>
! can be replaced by :
! INTEGER, DIMENSION(2) :: ik
! INTEGER :: ii,jj,Ndays
! ik(:)=Compute_Gmatij_ik(ii,Ndays)
! Gmatij=Compute_Gmat(ik(:),jj)

!------------------------------------------------------------------------------
  FUNCTION Compute_Gmatij_ik(ii,Nd) RESULT(ik)
    IMPLICIT NONE
    INTEGER :: ii,Nd
    INTEGER :: itmp
    INTEGER, DIMENSION(2) :: ik  ! 1: the Gmat subwindow
                                 ! 2: local i index in the Gmat subwindow
    ik(1)=0
    itmp=ii
    DO WHILE ( itmp > 0 )
      ik(1)=ik(1)+1
      ik(2)=itmp
      itmp=itmp-(Nd-ik(1))
    ENDDO
    RETURN
  END FUNCTION Compute_Gmatij_ik
  
!------------------------------------------------------------------------------
  FUNCTION Compute_Gmatij_ik0(ii,Nd,i0,is) RESULT(ik)
    IMPLICIT NONE
    INTEGER :: ii,Nd,i0,is
    INTEGER :: itmp
    INTEGER, DIMENSION(2) :: ik  ! 1: the Gmat subwindow
                                 ! 2: local i index in the Gmat subwindow
    ik(1)=i0-1
    itmp=ii-is
    DO WHILE ( itmp > 0 )
      ik(2)=itmp
      ik(1)=ik(1)+1
      itmp=itmp-(Nd-ik(1))
    ENDDO
    RETURN
  END FUNCTION Compute_Gmatij_ik0

!------------------------------------------------------------------------------
  FUNCTION Compute_Gmat(ik,jj) RESULT(Gmatij)
    IMPLICIT NONE
    INTEGER, DIMENSION(2) :: ik
    INTEGER :: jj
    INTEGER :: Gmatij
    IF ( jj==ik(1) ) THEN
      Gmatij=-1
    ELSEIF ( ( jj > ik(1) ) .AND. ( jj == ik(2)+ik(1) ) ) THEN
      Gmatij=1
    ELSE
      Gmatij=0
    ENDIF
    RETURN
  END FUNCTION Compute_Gmat
  
!------------------------------------------------------------------------------
  FUNCTION Compute_Gmatf(ik,jj) RESULT(Gmatij)
    IMPLICIT NONE
    INTEGER(KIND=1), DIMENSION(2,1) :: ik
    INTEGER :: jj
    INTEGER :: Gmatij
    IF ( jj==ik(1,1) ) THEN
      Gmatij=-1
    ELSEIF ( jj > ik(1,1) ) THEN
      IF ( jj == ik(2,1)+ik(1,1) ) THEN
        Gmatij=1
      ELSE
        Gmatij=0
      ENDIF
    ELSE
      Gmatij=0
    ENDIF
    RETURN
  END FUNCTION Compute_Gmatf
  
!------------------------------------------------------------------------------
  SUBROUTINE lscov2kindinvdt2(N,iNm1,A,B,W,rdt,rstdx,LWORK)
  ! cette subroutine n'est utile que si on veut direct inv(dtot_error)
    IMPLICIT NONE
    INTEGER, INTENT(in) :: N
    REAL(MYKIND), INTENT(in) :: iNm1  ! 1/(N-1)
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: A,B
    REAL(MYKIND), DIMENSION(N), INTENT(in) :: W
    REAL(MYKIND), INTENT(out) :: rdt,rstdx
    REAL(MYKIND) :: sW
    INTEGER :: ii
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: WORK ! working array for DGELS
!$omp threadprivate(work) 
    REAL(MYKIND), DIMENSION(:), ALLOCATABLE,SAVE :: x,y 
!$omp threadprivate(x,y)
    INTEGER :: INFO
    INTEGER, INTENT(in) :: LWORK
    if (.not. allocated(work)) then
      ALLOCATE(WORK(LWORK)) ! a ete determine une fois avec LWORK=-1
      ALLOCATE(x(N),y(N))
    endif
    DO ii=1,N
      sW=SQRT(W(ii))
      x(ii)=sW*A(ii)
      y(ii)=sW*B(ii)
    ENDDO
! See http://www.netlib.org/lapack/lawnspdf/lawn193.pdf section 4.1.2 to compute stdx as sqrt(c_ii)
    !CALL DGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
#ifdef lk_float
    CALL SGELS( 'N', N, 1, 1, x, N, y, N, WORK, LWORK, INFO )
#else
    CALL DGELS( 'N', N, 1, 1, x, N, y, N, WORK, LWORK, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    rdt=y(1)
    sW=ZERO
    DO ii=2,N
      sW=sW+y(ii)*y(ii)
    ENDDO
    rstdx=x(1)/SQRT(sW*iNm1)
  END SUBROUTINE lscov2kindinvdt2

!------------------------------------------------------------------------------
  SUBROUTINE compute_misfit(npit,ngit,dtoti,dtot1,misfit)
  ! calcul misfit = rms(G*dtot-data)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: npit,ngit
    REAL(MYKIND), DIMENSION(npit), INTENT(IN) :: dtot1
    REAL(MYKIND), DIMENSION(ngit), INTENT(IN) :: dtoti
    REAL(KIND=4), DIMENSION(1), INTENT(out) :: misfit
    INTEGER :: ii,jj,Gmatij
    REAL(MYKIND) :: RES,tmpii
    INTEGER, DIMENSION(2) :: tmpik
    RES=ZERO
    DO ii=1,npit
      tmpii=ZERO
      tmpik(:)=Compute_Gmatij_ik(ii,ngit)  ! Get the ik subwindow and the iiloc,on peut utiliser le ik0
! on a subwindow et iloc, on sait parfaitement quels sont les deux seuls jj diff de 0
    DO jj=1,ngit   ! on peut parcourir la table ?
      Gmatij=Compute_Gmat(tmpik(:),jj)    ! Get the Gmat local value
      tmpii=tmpii+Gmatij*dtoti(jj)
    ENDDO
    RES=RES+(tmpii-dtot1(ii))**DEUX
  ENDDO
  misfit(1)=REAL(SQRT(RES/npit))
  END SUBROUTINE compute_misfit

!------------------------------------------------------------------------------
  SUBROUTINE compute_ApostError(ngit,AP,ApostError)
    ! Computes apostError = diag(Cmpost) [N]
    ! use dtptri to compute the inverse of a symmetric (saved as triangular) matrix
    ! -> computes CmPost
    ! pas compris pkoi eleve au carre ?
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ngit
    REAL(MYKIND), DIMENSION(ngit*(ngit+1)/2), INTENT(inout) :: AP
    REAL(KIND=4), DIMENSION(ngit), INTENT(out) :: ApostError
    INTEGER :: ib,ii,jj,kk,INFO
    REAL(MYKIND) :: rsomme
#ifdef lk_float
    CALL STPTRI( 'U', 'N', ngit, AP, INFO )
#else
    CALL DTPTRI( 'U', 'N', ngit, AP, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    ib=0
    DO ii=1,ngit
      ib=ib+ii
      kk=ib
      rsomme=ZERO
      DO jj=0,ngit-ii
        rsomme=rsomme+AP(kk)**DEUX
        kk=kk+ii+jj
      ENDDO
      ApostError(ii)=REAL(rsomme)
    ENDDO
  END SUBROUTINE compute_ApostError

!------------------------------------------------------------------------------
  SUBROUTINE compute_resolution(ngit,npit,dtot2,AP,resolution)
    ! Calcul de resolution = diag( ( (Gt * Cd-1 * G + Cm-1)-1 * Gt .* Cd-1 ) * Gmat ) [N]
    ! pas compris cette version du calcul de resolution ?
    ! create B2full =  Gt .* Cdinv * G    [N,N]
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ngit,npit
    REAL(MYKIND), DIMENSION(npit), INTENT(IN) :: dtot2
    REAL(MYKIND), DIMENSION(ngit*(ngit+1)/2), INTENT(IN) :: AP
    REAL(KIND=4), DIMENSION(ngit), INTENT(OUT) :: resolution
    REAL(MYKIND), DIMENSION(ngit,ngit) :: B2full
    INTEGER :: kk,jj,ii,INFO
    REAL(MYKIND) :: rsomme
    kk=0
    DO jj=1,ngit
    ! on construit lower de B2full
      DO ii=jj+1,ngit
        kk=kk+1
        B2full(ii,jj)=-dtot2(kk)
        ! upper <- symmetric matrix
        B2full(jj,ii)=B2full(ii,jj)
      ENDDO
    ENDDO
    ! on construit la diagonal (somme des elements des colonne)
    DO jj=1,ngit
      B2full(jj,jj)=ZERO
      rsomme=ZERO
      DO ii=1,ngit
        rsomme=rsomme+B2full(ii,jj)
      ENDDO
      B2full(jj,jj)=-rsomme
    ENDDO
    ! Resolve AX=B2full
    ! we re-use the cholesky decomposition of A
    ! B2full is updated
#ifdef lk_float
    CALL SPPTRS( 'U', ngit, ngit, AP, B2full, ngit, INFO )
#else
    CALL DPPTRS( 'U', ngit, ngit, AP, B2full, ngit, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    ! take the diagonal
    DO ii=1,ngit
      resolution(ii)=REAL(B2full(ii,ii))
    ENDDO
  END SUBROUTINE compute_resolution

!------------------------------------------------------------------------------
  SUBROUTINE compute_dvov(ngit,npit,dtot1,dtot2,MatCmP,AP,dvov)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ngit,npit
    REAL(MYKIND), DIMENSION(npit), INTENT(IN) :: dtot1,dtot2
    REAL(MYKIND), DIMENSION(ngit*(ngit+1)/2), INTENT(IN) :: MatCmP
    REAL(MYKIND), DIMENSION(ngit*(ngit+1)/2), INTENT(OUT) :: AP
    REAL(MYKIND), DIMENSION(ngit), INTENT(OUT) :: dvov
    INTEGER, DIMENSION(ngit) :: table  ! table of integer containing the number of previous line in Gmat table
    INTEGER, DIMENSION(ngit,ngit-1) :: table_valid_idx_diag ! the valid index along npit dimension (for each ngit value)
    INTEGER :: somme,ii,jj,it,kk3,kk,INFO
    REAL(MYKIND) :: Mat2ij
    ! Create a table (ngit) of integer containing the number of previous line in Gmat table
    ! Create a table_valid_idx_diag: the valid index along npit dimension (for each ngit value)
    somme=0
    DO ii=1,ngit
      table(ii)=somme
      somme=somme+(ngit-ii)
      table_valid_idx_diag(ii,1)=ii-1
      it=0
      ! This loop is executed only for ii=3+
      DO jj=2,ii-1
        it=it+1
        table_valid_idx_diag(ii,jj)=table_valid_idx_diag(ii-1,jj-1)+(ngit-it) ! Here ii>=3
      ENDDO
      it=0
      DO jj=ii,ngit-1
        it=it+1
        table_valid_idx_diag(ii,jj)=table(ii)+it
      ENDDO
    ENDDO
    ! Compute AP = inv(Cmpost) = Gt * Cd^(-1) .* G + Cm^(-1)
    ! Or Cmpost est symetrique donc on ne stocke qu'un triangle avec sa diagonale (N(N+1)/2)
    kk3=0
    DO ii=1,ngit
      ! 2.
      Mat2ij=ZERO
      DO kk=1,ngit-1
        Mat2ij=Mat2ij+dtot2(table_valid_idx_diag(ii,kk))
      ENDDO
      AP((ii+1)*ii/2)=Mat2ij+MatCmP((ii+1)*ii/2)
      ! 3.
      DO jj=ii+1,ngit
        kk3=kk3+1
        Mat2ij=-dtot2(kk3) ! Cdinv(kk)
        AP(ii + (jj-1)*jj/2)=Mat2ij+MatCmP(ii + (jj-1)*jj/2)
      ENDDO
    ENDDO
    ! Create the right hand side dtoti = Gt .* Cd^(-1) * data [N]
    dvov(:)=ZERO
    DO ii=1,ngit
      DO jj=1,ii-1
        dvov(ii)=dvov(ii)+dtot2(table_valid_idx_diag(ii,jj))*dtot1(table_valid_idx_diag(ii,jj))
      ENDDO
      DO jj=ii,ngit-1
        dvov(ii)=dvov(ii)-dtot2(table_valid_idx_diag(ii,jj))*dtot1(table_valid_idx_diag(ii,jj))
      ENDDO
    ENDDO
    ! Resolve model = ( Gt * Cd^(-1) .* G + Cm^(-1) )^(-1) * ( Gt .* Cd^(-1) * data )
    !         dtot  =                  AP                  *         dtoti
#ifdef lk_float
    CALL SPPSV('U', ngit, 1, AP, dvov, ngit, INFO )
#else
    CALL DPPSV('U', ngit, 1, AP, dvov, ngit, INFO )
#endif
    if (INFO /= 0) call exit(INFO)
    ! xPPSV updates dvov
!!!!!!!!!!!!
  END SUBROUTINE compute_dvov 

END MODULE inversionmodule
