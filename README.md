Whisper and F-IMAGE projects
============================

Accès aux pages publiques
-------------------------

https://lecoinal.gricad-pages.univ-grenoble-alpes.fr/doublets

---

This is the code to compute:
- correlations
- doublets
- and invert doublets

This repository comes from previous SVN repository for doublets project, which is no more maintained:

```
Path: .
Working Copy Root Path: /home/lecointre/DOUBLETS
URL: https://forge.osug.fr/svn/isterre-correlation/DOUBLETS
Relative URL: ^/DOUBLETS
Repository Root: https://forge.osug.fr/svn/isterre-correlation
Repository UUID: fb781e4e-3e15-492d-9926-ae8842bad7eb
Revision: 252
Node Kind: directory
Schedule: normal
Last Changed Author: lecointre
Last Changed Rev: 249
Last Changed Date: 2018-02-06 15:44:47 +0100 (Tue, 06 Feb 2018)
```

Here is the previous [Changelogs from this previous repository](./changelog_previous_svn_repos.txt)

---

Related wikis:

[Wiki for Whisper project](https://forge.osug.fr/trac/ISTERRE-Correlation)

and especially the page about [doublets measurement](https://forge.osug.fr/trac/ISTERRE-Correlation/wiki/CorrDoublets)

---

Compile with GuiX environment on GriCAD clusters

We use the [GUIX manifest file manifest_doublets.scm](./manifest_doublets.scm)

1. Create the doublets environment

```
> source /applis/site/guix-start.sh
> refresh_guix doublets
> guix package -m manifest_doublets.scm -p $GUIX_USER_PROFILE_DIR/doublets
> refresh_guix doublets
```

2. Compile

```
> source /applis/site/guix-start.sh
> refresh_guix doublets
> make -f Makefile_guix
```

