#!/usr/bin/env python
# -*- coding: utf-8 -*-

# this method should be changed with 
# 1) virtual dataset method
# 2) clean 0 data

import h5py
import sys
import numpy as np
import os

min_hours = 20 # if there are less than min_hours valid hours, delete the entire day

#datatype=np.int8
datatype=np.float32

idir='.'
#year=int(sys.argv[1])
#jd=int(sys.argv[2])
ifile=sys.argv[1] # 'R0101_EPZ.h5'
odir='./out/.'
ofile=os.path.basename(ifile) # 'R0101_EPZ.h5'

freq=20
#ltf=3600*freq
ltf=86400*freq
st=np.empty((ltf,), dtype=datatype)  # 8bit integer

h5f = h5py.File(idir+'/'+ifile,'r')
h5fo = h5py.File(odir+'/'+ofile,'w')

# loop over jd into h5file
for year in h5f:
  for jd, data in h5f[year].items():
    jjd = str("{:03}".format(int(jd)))

    # check that at least 20 hours are OK. if not, deleting the entire day
    nbvhd = 24
    for ih in range(24):
      start=ih*3600*freq
      maxabs=np.max(np.abs(data[start:start+3600*freq]))            
      if maxabs == 0:
        nbvhd -= 1
    if nbvhd >= min_hours:
      st[:]=data[:]
      h5fo.create_dataset(year+'/'+jjd,shape=(ltf,), dtype=datatype,data=st[:], shuffle='true', compression='gzip', compression_opts=6, fletcher32='True')
    else:
      print(jd,data,"less than 20 valid hours in the day:",nbvhd)			

h5f.close()
h5fo.close()
