#ifndef types_h
#define types_h
#ifdef lk_float
#  define MYKIND C_FLOAT
#  define MYKINDCPLX C_FLOAT_COMPLEX
#  define ZERO 0.0
#  define UN 1.0
#  define ZEROCINQ 0.5
#  define DEUX 2.0
#else
#  define MYKIND C_DOUBLE
#  define MYKINDCPLX C_DOUBLE_COMPLEX
#  define ZERO 0.d0
#  define UN 1.d0
#  define ZEROCINQ 0.5d0
#  define DEUX 2.d0
#endif
#endif
