(specifications->manifest
  '("gfortran-toolchain"
    "gcc-toolchain"
    "lapack"
    "openblas"
    ;; single precision FFT computation
    ;;"fftwf"
    ;; double precision FFT computation
    "fftw"
    "hdf5"
    "hdf5:fortran"
    "zlib"
    "python@3"
    "python-h5py"
    "python-scipy"
    ))
