!#---------------------------------
!$Id: compute_Cminv.f90 240 2017-09-20 12:00:55Z lecointre $
!#---------------------------------

!###############################################################################
!#    This file is part of doublets projects : Fortran + python codes for      #
!#    computing correlations, measuring doublets, and invert it                #
!#                                                                             #
!#    Copyright (C) 2019 Albanne Lecointre, ...                                #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

PROGRAM compute_Cminv

!===========================================================!
! This program computes the inverse matrix Cminv as         !
! Cminv(i,j) = exp( -(dist(i,j)) / (2*Lcorr) )              !
! avec Lcorr en jours represente la longueur de correlation !
! Cm and Cminv are symetric band Topelitz matrix.           !
! This program stores Cminv in packed storage and normal    !
! format.                                                   !
! Cminv [N,N] is a covariance matrix on model params built  !
! from distance between model params. It represents the     !
! temporal smoothing of the final dt/t curve.               !
!===========================================================!

! $ sv2
! $ module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1
! $ make compute_Cminv
! $ h5pfc -warn all -check all -cpp -Dlk_1bit  -O2  -o compute_Cminv compute_Cminv.f90 /applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib/libgfortran.so.3 -L/applis/ciment/v2/stow/x86_64/gcc_4.6.2/lapack_3.3.0/ -llapack -lblas


  USE HDF5 ! HDF5 module
  USE H5LT ! Lite Interface HDF5 : wrapper
  USE corrmodule

#include "types.h"

  USE, intrinsic :: iso_c_binding

  IMPLICIT NONE

  LOGICAL :: l_exist_namelist

  INTEGER :: N ! can be deduced from nn_datebeg, nn_dateend, nn_mcorr

  INTEGER :: njday,nstep_ifnogap

  CHARACTER(LEN=8) :: cN,cL,cP
  INTEGER :: ii,jj              ! dummy loop indices 
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: AP
!  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: A
  REAL(MYKIND) :: inv_2_Lcorr  ! 1/(2*nn_Lcorr)

  ! I/O
  INTEGER :: INFO
  INTEGER(HID_T) :: outfile_id
  INTEGER(HSIZE_T), DIMENSION(1) :: outdims ! = (/N*(N+1)/2/) 
!  INTEGER(HSIZE_T), DIMENSION(2) :: outdims2 ! = (/N,N/)
  INTEGER :: error

  INTEGER :: nn_datebeg,nn_dateend,nn_mcorr,nn_Lcorr,nn_pondcoef
  LOGICAL :: ln_inversion,ln_dailymean

  ! Declare NAMELIST
  NAMELIST /namtim/ nn_datebeg,nn_dateend,nn_mcorr 
  NAMELIST /naminversion/ ln_inversion,nn_Lcorr,nn_pondcoef 

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
  PRINT *,'Reading namelist namtim'
    REWIND 11
    READ(11,namtim)
    PRINT *,'nn_datebeg: ',nn_datebeg
    PRINT *,'nn_dateend: ',nn_dateend
    IF (nn_mcorr .GE. 0) THEN
      ln_dailymean = .false.
      PRINT *,'nn_mcorr: ',nn_mcorr,'-hour averaged correlations'
      IF ( (nn_mcorr /= 1) .AND. (nn_mcorr /= 2) &
                           .AND. (nn_mcorr /= 3) &
                           .AND. (nn_mcorr /= 4) &
                           .AND. (nn_mcorr /= 6) &
                           .AND. (nn_mcorr /= 8) &
                           .AND. (nn_mcorr /= 12)  ) THEN 
        print *,'valid values are 1,2,3,4,6,8,12 for hourly decimation: Abort ...'
        call EXIT(1)  ! Cigri sould abort
      ENDIF
    ELSE
      nn_mcorr = -nn_mcorr
      ln_dailymean = .true.
      PRINT *,'nn_mcorr: ',nn_mcorr,'-day averaged correlations'
    ENDIF
  PRINT *,'Reading namelist naminversion'
    REWIND 11
    READ(11,naminversion)
    PRINT *,'ln_inversion: ',ln_inversion
    PRINT *,'nn_Lcorr: ',nn_Lcorr
    PRINT *,'nn_pondcoef: ',nn_pondcoef

  CLOSE(11)

  ! Compute some useful parameters

  ! Find njday (all days, valid or not) starting from datebeg and dateend
  njday = FIND_NJDAY(nn_datebeg,nn_dateend)

  if ( ln_dailymean ) then
    nstep_ifnogap = njday/(nn_mcorr)
  else
    nstep_ifnogap = 24*njday/nn_mcorr
  endif
  N = nstep_ifnogap

  outdims = (/N*(N+1)/2/) 
!  outdims2 = (/N,N/)

  ! Storage in packed format
  ALLOCATE(AP(N*(N+1)/2))
  inv_2_Lcorr = UN / (DEUX*nn_Lcorr)
  DO ii=1,N
    DO jj=ii,N
      AP(ii + (jj-1)*jj/2) = EXP( -ABS( (jj-ii)*inv_2_Lcorr ) )
    ENDDO
  ENDDO

#ifdef lk_float
  CALL SPPTRF( 'U', N, AP, INFO )
#else
  CALL DPPTRF( 'U', N, AP, INFO )
#endif
  if (INFO /= 0) call exit(INFO)
#ifdef lk_float
  CALL SPPTRI( 'U', N, AP, INFO )
#else
  CALL DPPTRI( 'U', N, AP, INFO )
#endif
  if (INFO /= 0) call exit(INFO)

  AP(:) = nn_pondcoef * AP(:)

!  ALLOCATE(A(N,N))
!  ! if UPLO = 'U', AP(i + (j-1)*j/2) = A(i,j) for 1<=i<=j;
!  DO jj=1,N
!    DO ii=1,jj
!      A(ii,jj)=AP(ii + (jj-1)*jj/2)
!    ENDDO
!  ENDDO
!  DO jj=1,N
!    DO ii=jj+1,N
!      A(ii,jj)=A(jj,ii)
!    ENDDO
!  ENDDO

  CALL h5open_f(error)
  if (error /= 0) call exit(error)
  write(cN,'(I0.8)') N
  write(cL,'(I0.8)') nn_Lcorr
  write(cP,'(I0.8)') nn_pondcoef
  CALL h5fcreate_f('Cminv_N'//TRIM(cN)//'_L'//TRIM(cL)//'_P'//TRIM(cP)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
  if (error /= 0) call exit(error)
#ifdef lk_float
  CALL h5ltmake_dataset_float_f(outfile_id, '/Cminv_packed_storage', 1, outdims, AP(:), error)
!  CALL h5ltmake_dataset_float_f(outfile_id, '/Cminv', 2, outdims2, A(:,:), error)
#else
  CALL h5ltmake_dataset_double_f(outfile_id, '/Cminv_packed_storage', 1, outdims, AP(:), error)
!  CALL h5ltmake_dataset_double_f(outfile_id, '/Cminv', 2, outdims2, A(:,:), error)
#endif
  if (error /= 0) call exit(error)
  CALL h5fclose_f(outfile_id, error)
  if (error /= 0) call exit(error)
  CALL h5close_f(error)
  if (error /= 0) call exit(error)

END PROGRAM compute_Cminv
