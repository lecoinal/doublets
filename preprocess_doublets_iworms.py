#!/usr/bin/env python
# -*- coding: utf-8 -*-

###############################################################################
#    This file is part of doublets : a python code for preprocessing          #
#    SCEDC (CI network) dataset : spectral whitening + 1bit filtering         #
#                                                                             #
#    Copyright (C) 2023 Albanne Lecointre                                     #
#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
###############################################################################

#  =======================================================================================
#                       ***  Python script preprocess_doublets_iworms.py  ***
#   This is the main Python script to compute the final preprocessing on gap-filled and 
#   decimated SCEDC HDF5 data
#   This script reads input as HDF5 files
#    
#  =======================================================================================
#   History :  1.0  : 06/2023  : A. Lecointre : Initial Python version
#               
#  ---------------------------------------------------------------------------------------
#
#  ---------------------------------------------------------------------------------------
#   methodology   : whitening 0.08-8Hz
#                 : 1-Bit
#
#
#  ---------------------------------------------------------------------------------------

import numpy as np
import time
import sys
import h5py

t0=time.time()

year=int(sys.argv[2]) # 2010 # int(sys.argv[2])
#jd=int(sys.argv[2])

option_white=1 # int(sys.argv[3]) # 1 # 1: whitening / other: bandpass
option_sig=1 # int(sys.argv[4])   # 1 # 1: 1Bit / 2: rms clip (4xrms) / other: do nothin

#frequences_corr=np.array([float(sys.argv[5]),float(sys.argv[6])],np.float64)  # 0.08 8 : frequence for whitening or bandpass
frequences_corr=np.array([0.08,8.0],np.float64)  # 0.08 8 : frequence for whitening or bandpass
deltaf=0.01 # float(sys.argv[7])       # 0.2 : for whitening

Fsnew=20 # int(sys.argv[8])        # 100 : current sampling frequency

##############################################################
# code for Final preprocessing debore correlation

ltf=86400*Fsnew

idir='/bettik/maos/dataBackup/0_preProcessedData/run1_dt/'+str(year)+'/'
odir='./'
ifile=sys.argv[1] # 'CI.BRE.__.Z'

freq=np.arange(0,Fsnew,np.float64(Fsnew)/ltf)

J=np.where((freq > frequences_corr[0]) & (freq < frequences_corr[1]))
Jdebut=np.where((freq > frequences_corr[0]) & (freq<(frequences_corr[0]+deltaf)))
Jfin=np.where((freq>(frequences_corr[1]-deltaf)) & (freq<frequences_corr[1]))

# Read from HDF5 dataset (to be done : read from mseed dataset)
h5f = h5py.File(idir+'/'+ifile+'.h5','r') #   SG_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'.h5','r')
h5fo = h5py.File(odir+'/'+ifile+'.trace.int8.h5','a') # SG_'+"{:04d}".format(year)+'_'+"{:03}".format(jd)+'.trace.int8.h5','w')

print("Init time: ",time.time()-t0,' sec')

# Loop over the stations for the final preprocessing
inode=0
for node in h5f[str(year)]:
	t0=time.time()

	inode+=1
	intnode=int(node)

	st=np.empty((ltf,), dtype=float)         # 8640000,nbtrace  -> 8640000
	st_preproc=np.zeros_like(st)

	st[:]=h5f[str(year)+'/'+node]

	if option_white==1:
		import scipy.fftpack
		st_preproc=np.zeros((ltf,),np.complex128)
		fftst=scipy.fftpack.fft(st[:])
		st_preproc[J]=fftst[J]/abs(fftst[J])
		if np.size(Jdebut)>1:
			st_preproc[Jdebut]=st_preproc[Jdebut]*(np.sin(np.pi/2*np.arange(0,np.size(Jdebut))/(np.size(Jdebut)-1))**2)
		if np.size(Jfin)>1:
			st_preproc[Jfin]=st_preproc[Jfin]*(np.cos(np.pi/2*np.arange(0,np.size(Jfin))/(np.size(Jfin)-1))**2)
		st_preproc=2*scipy.fftpack.ifft(st_preproc).real
	else:
		from scipy.signal import filtfilt, butter
		# Butterworth digital filter design
		BB, AA = butter(3,frequences_corr/Fsnew*2,btype='bandpass')
		st_preproc = filtfilt(BB, AA, st[:])

	if option_sig==1:
		st_preproc=np.sign(st_preproc)
		h5fo.create_dataset(str(year)+'/'+"{:03d}".format(intnode),shape=(ltf,), dtype='i1',data=st_preproc[:].astype('int8'),compression='gzip', compression_opts=1, fletcher32='True')
	elif option_sig==2:
		rms4=4*np.std(st_preproc)
		np.clip(st_preproc, -rms4, rms4, out=st_preproc)
		h5fo.create_dataset(str(year)+'/'+"{:03d}".format(intnode),shape=(ltf,), dtype='float32',data=st_preproc[:].astype('float32'),compression='gzip', compression_opts=1, fletcher32='True')

	print(inode,node,time.time()-t0," sec")

h5f.close()
h5fo.close()

