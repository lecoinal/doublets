DONE:

! rebrancher l'inversion en partant de r109 : OK

! add script compute cminv en partant de cmpost de rlast : ok : pas bande ? / sensible a float/dble

! exit si pb INFO : OK

nn_pondcoef, nn_Lcorr ngit lu dans namelist par cminv.f90 : OK

verifier des resultats de I01 ou I02 : OK I01.S

aussi de D0X : OK D00.S

attention normalisation des corr peut expliquer des modifs : non norm des corr ne modifie pas du tout les dtot et dvov

copile check all warn all result different ? -> non c'est OK si on est en DOUBLE, seul Cminv differe mais ca ne diffère pas le t'inversion finale

voir lk_float ou non ? --> il FAUT calculer en DOUBLE

! lecture namelist que si ln_inv ou ln_doublets

ln_dailymean : pas n car pas namelist

! verifier toutes les ouv/ferm de input file et output file

! ne remet pas float, n'autorise que double : ok

! regarder toutes les diff (rnew) avec (r>109 jusqu'a rlast) : ok

remove int -> dble when not necessary

indentation

voir division dans les boucles, y en a dans les corr

propset

modulariser !!!

to write dcorr, il faut rdcorr = real(dcorr) car dcorr a le statut target et on ne peut pas passer real(dcorr) a h5ltmakedatasetfloat
resoud pr segfault a write dcorr incompris

faire des float direct pour les var qu'on écrit dans les h5file et qu'on reutilise pas aprs : ok avec 4 refs


---------------------------------------
TODO:

nettoyer toutes les var, nommage, ...
remet omp ?
voir matmul
comprendre les calcul resol, etc ... non compris
invetsiguer float/double ?

si 20days et nn_mcorr=-1, pas de diff, sauf Cminv mais ca n'a pas d'impact sur dvov.h5
voir avec matlab la superposition des 2 cminv avec imagesc ? : 3352112 et 3352110
revoir sur 731 jours et EXPI01.S...




-------------------------------------------------------
reference
2008220-2008239
nn_Lcorr=5
 cn_st(1): AHIH
 cn_st(2): ARKH
 cn_ch(1): BAE
 cn_ch(2): BAN                  avec dvov et dtot'
3334447  nn_mcorr=-1            3348622
3334463  -2                     3348854
3334458  1                      3351970 
3334459  12                     3348879

0.08-2
GKSH MRTH SHE SHZ
GKSH SHE MRTH SHZ           18.232280
2011-2012
EXPI01.S Lcorr=1000
nn_Lcorr=-1                     3348735  : ok id EXPD00.S et EXPI01.S


