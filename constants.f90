!#---------------------------------
!$Id: constants.f90 92 2015-03-16 09:45:11Z lecointre $
!#---------------------------------

MODULE constants

  USE, intrinsic :: iso_c_binding

#include "types.h"

  IMPLICIT NONE

#ifdef lk_float
  REAL(MYKIND), PARAMETER :: pi = 4.0*ATAN(1.0)
  REAL(MYKIND), PARAMETER :: mpi = -4.0*ATAN(1.0)
  REAL(MYKIND), PARAMETER :: pi2 = 8.0*ATAN(1.0)
  REAL(MYKIND), PARAMETER :: invpi2 = 1.0 / (8.0*ATAN(1.0))
#else
  REAL(MYKIND), PARAMETER :: pi = 4.d0*DATAN(1.d0)
  REAL(MYKIND), PARAMETER :: mpi = -4.d0*DATAN(1.d0)
  REAL(MYKIND), PARAMETER :: pi2 = 8.d0*DATAN(1.d0)
  REAL(MYKIND), PARAMETER :: invpi2 = 1.d0 / (8.d0*DATAN(1.d0))
#endif

END MODULE constants
