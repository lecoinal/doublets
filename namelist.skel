&namtim
  nn_datebeg = 2010001 ! 2013152 ! 2010084 ! 2011001 ! 2008220          ! Beginning julian day (format YYYYJJJ)
  nn_dateend = 2010031 ! 2013365 ! 2010105 ! 2012366 ! 2008239          ! Ending julian day (format YYYYJJJ)
  nn_mcorr = 12                ! if positive: to decrease correlation frequency inside the current day : 1: no decimation: keep hourly correlation
                                !                                                                         2,3,4,6,8,12: 2h-corr, 3h-corr, ..., 12h-corr
                                ! if negative: to decrease correlation frequency over several days : -1: daily corr
                                !                                                                    -15: 15days-corr (nn_datebeg and nn_dateend should be n*15 otherwise it cuts at the end)
/
&nambp
  cn_file1 = "<<FILE1>>"          ! input file names (without .h5 suffix), must be updated by submission script
  cn_file2 = "<<FILE2>>"
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for correlations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namparam2d
! this namelist section works only if compiling with lk_2Dinput
  ln_corr = .true.
  cn_prefixdir = "" ! "2010"
  nn_samprate = 20 ! 10 ! 20              ! Input sampling rate (Hz)  ! INTEGER
  nn_unitseg = 180 ! 3600 ! 1200             ! The number of seconds in one unitary segment for the correlation ex:20min=1200sec
  nn_nbunitseg = 40 ! 1 ! 5              ! The number of unitary segment in one hour correlation
  sn_lag = 100 ! 60 ! 400                  ! The lag in sec (+/-)      ! REAL but maxlag = sn_lag * nn_samprate must be an integer !
  nn_nextpow2 = 0
  ln_writecorr = .true.         ! flag to write correlations or not
  ln_writedcorr = .false.     ! flag to write correlations as dble precision (obligatoire dans le cas ou l'on souhaite redemarrer avec ln_corr=false)
/
&namparam
  ln_corr = .true. ! .false.
  cn_prefixdir = "" ! "2010" ! only of ln_corr = false : indicate the prefix dir (H5 group) where are correlation datasets
                             ! if there is no group : "" (without any space)
                             ! several group is OK : "2010/abs/005" (max 256 characters)
  nn_samprate = 5 ! 10 ! 20              ! Input sampling rate (Hz)  ! INTEGER
  nn_unitseg = 180 ! length (sec) of input data for one time ste computation
  nn_overlap = 90  ! timestep duration (sec)
  nn_iter = 39 ! 959 ! 39 ! 3min with 1.5min overlap : we have 39 in one hour or 959 in one day
  sn_lag = 100 ! 150 ! 100 ! 60 ! 400     ! The lag (REAL) in sec (+/-) for which correlation is computed (if ln_corr=T) of for the correlation in the input file      ! REAL but maxlag = sn_lag * nn_samprate must be an integer !
  nn_nextpow2 = 0 ! 8192      ! if 0, npad=nph+maxlag=(nn_unitseg+nn_lag)*nn_samprate                 
                          ! INTEGER
                          ! but it could be more efficient to choose the next pow 2 value
                          ! FFTW is best at handling sizes of the form 2^a 3^b 5^c 7^d 11^e 13^f,
                          ! where e+f is either 0 or 1, and the other exponents are arbitrary.
  ln_writecorr = .true.         ! flag to write correlations or not
  ln_writedcorr = .true.     ! flag to write correlations as dble precision (obligatoire dans le cas ou l'on souhaite redemarrer avec ln_corr=false)
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for doublets
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namdoublet
  ln_doublets=.true.  ! Flag to compute doublets
  nn_dt_ref=0       ! Option flag for computing doublets :
                    ! -1        : calculating doublets between every two core and then doing inversion
                    !  0        : calculating the doublet of each corr with regard to a reference: average (implies ln_inversion=F)
                    !  i=1-nstep: calculating the doublet of each corr with regard to a reference: timestep i (implies ln_inversion=F)
  dist=<<DIST>>       ! interdistance in km
  Fmin=0.5 ! 0.08d0 ! 0.15d0 ! 0.033d0
  Fmax=1.0 ! 2.d0 ! 0.9d0 ! 0.125d0          ! MUST BE < Fs/2
  LWin=5.0 ! 10.0 ! 5.0 ! 10.d0 ! 30.d0          ! Window length (in seconds)
  distOverTmin=0.0 ! -17.5 ! 2.0 ! -17.5 ! 2.2d0    ! IF POSITIVE : velocity (in km/sec) to estimate the direct arrival travel time (should be lowered compared to reality because end of window is Tmin)
                                ! Tmin is computed as Tmin = dist / distOverTmin
                                ! IF NEGATIVE or ZERO : -Tmin in sec
  Tmax=100.0 ! 32.5 ! 123.85 ! 123.84 ! 32.5 ! 100.0 ! 60.d0 ! 400.d0          ! this value MUST be EQUAL to sn_lag parameter
  Overlap=0.d0 ! -100.0 ! 70.0 ! 0.0 ! 70.0 ! 0.0 ! 70.0 ! 80.d0 ! 60.d0       ! overlap en pourcent
  Fs=5.0 ! 10.d0            ! sampling frequency
  ln_writedtmat = .true. ! .false.
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for inversion
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&naminversion
  ln_inversion=.false. ! .true.  ! Flag to invert doublets
  nn_Lcorr=1 ! 1000 ! 5
  nn_pondcoef=1 ! 100000 ! 1000
/
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Parameters for OMP threads
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&namscheduling
  sn_chunksizeparam=0.05
/
