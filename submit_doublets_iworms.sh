#!/bin/bash
#OAR -l /nodes=1/cpu=1/core=1,walltime=96:00:00
#OAR --project f-image
#OAR -n doublet
##OAR -t besteffort

#################

set -e

case "$HOSTNAME" in
  ist-156-112)
    echo $HOSTNAME
    WDIR="/tmp/$USER/"
  ;;
  *)   # gricad clusters : luke|dahu|bigfoot
    echo $HOSTNAME
    source /applis/site/guix-start.sh
    refresh_guix doublets
    WDIR="${SHARED_SCRATCH_DIR}/$USER/"
  ;;
esac

F90EXE=corr_doublet_inversion_bypair
#PEXE=prepare_input_dataset.py


#FILE1="CI.CHN.__.Z" 
#FILE2="CI.RVR.__.Z" 
#DIST=28.153726 # en km

#FILE1="CI.ERR.__.Z" 
#FILE2="CI.RXH.__.Z" 
#DIST=17.6728405364

#FILE1="CI.BRE.__.Z" # "CI.CIA.__.Z"   CI.BRE.00.Z CI.DLA.00.Z 
#FILE2="CI.DLA.__.Z" # "CI.CHN.__.Z" 
#DIST=11.550312518734689  # 20.059739 # en km
#FILE1="CI.BRE.__.Z"
#FILE2="CI.LLS.__.Z"
#DIST=14.12678310723991
FILE1="$1"  # "CI.LAF.__.Z"
FILE2="$2"  #"CI.LGB.__.Z"
DIST=$3 # 20.57029577662673

# CI.BRE.__.Z.trace.int8 CI.DLA.__.Z.trace.int8 11.550312518734689
# CI.BRE.__.Z.trace.int8 CI.LLS.__.Z.trace.int8 14.12678310723991
# CI.LAF.__.Z.trace.int8 CI.LGB.__.Z.trace.int8 20.57029577662673

# local : ./submit_doublets_iworms.sh CI.BRE.__.Z.trace.int8 CI.LLS.__.Z.trace.int8 14.12678310723991

ABS=0   # apply absolute value to coherence before computing iworms weighted dvov
AUTOCORR=0   # keep autocorr (diagonal coh = 1 and diagonal dt = 0) for computing iworms dvov and weigthed dvov

RESTART_OPT=1 # 2
# 0 : start from raw input and apply prepare_input_dataset.py (keep daily dataset by removing less than 20hours valid days)
# 1 : restart from these already prepared input file
# 2 : restart from correlation file : in that case ln_corr MUST be false
# you also have to fill the relevant INPUTDIR here :
case "$CLUSTER_NAME" in
  luke)
    #INPUTDIR="/bettik/lecointre/SHUJUAN/"  # for RESTART_OPT=0
    #INPUTDIR="/bettik/lecoinal/SHUJUAN/test/"  # 2010
    INPUTDIR="/bettik/lecoinal//oar.prepSHUJUAN.76736803"   # 2000-2020
    #INPUTDIR="/bettik/maos/dataBackup/0_preProcessedData/run1_dt/2010/"
#    INPUTDIR="/bettik/lecointre/oar.SHUJUAN.23312232/" # for RESTART_OPT=1
#    INPUTDIR="/bettik/lecointre/oar.SHUJUAN.23313209/" # for RESTART_OPT=2
    ;;
  ISTERRE)
#    INPUTDIR="/data/projects/f_image/maos/0_PRE_PROCESS/data_20hz/daily/run_luke2_SDS/2010/" # for RESTART_OPT=0
    INPUTDIR="/nfs_scratch/lecoinal/oar.SHUJUAN.625692/" # for RESTART_OPT=1 ou 2
    ;;
  *)  # si rien, c'est local
    INPUTDIR="/$HOME/DATA/tmp/"
esac

##########################################################
# try to do not change anything under this line...

getParam() {
   # Get a parameter value from namelist
   value=$(grep "^ *$1 *= *" namelist | sed 's/!.*$//' | awk -F= '{print $2}')
   echo $value
}

#########################################################

HERE=$(pwd)

#case "$CLUSTER_NAME" in
#  luke)
#    export LD_LIBRARY_PATH=/applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib:$LD_LIBRARY_PATH
#    ;;
#esac

# prevent segfault -> stacksize * 2
#ulimit -s 16384
#ulimit -s unlimited

#cat $OAR_NODE_FILE
#NBCORE=$(cat $OAR_NODE_FILE | wc -l)

TMPDIR="$WDIR/oar.SHUJUAN.$OAR_JOB_ID";        # 40TB

echo "TMPDIR: $TMPDIR"

mkdir -p $TMPDIR
cp $F90EXE $TMPDIR/.
#cp $PEXE $TMPDIR/.
cp dvov_iworms.py $TMPDIR/.
cp compute_Cminv $TMPDIR/.

sed -e "s/<<FILE1>>/$FILE1/" -e "s/<<FILE2>>/$FILE2/" -e "s/<<DIST>>/$DIST/" namelist_doublets_iworms.skel > $TMPDIR/namelist

#############################

cd $TMPDIR

ln_inversion=$(getParam ln_inversion)

#####################################

echo "Get input:                               Date and Time: $(date +%F" "%T"."%N)"

START=$(date +%s.%N)

if [[ "$RESTART_OPT" == "0" ]] ; then
  # Option 0
  # Start from "raw" data and prepare input dataset (with removing less than 20 valid hours day)
  case "$CLUSTER_NAME" in
    luke)
      PYV="python3" # from guix doublets profile
      ;;
    ISTERRE)
      module load python/python3.7
      PYV="python3"
      ;;
    *) # local
      PYV="python3"
  esac
  mkdir -p out
  ln -s $INPUTDIR/${FILE1}.h5 tmp.h5
  $PYV $PEXE tmp.h5
  mv out/tmp.h5 ${FILE1}.h5
  ln -s $INPUTDIR/${FILE2}.h5 tmp.h5
  $PYV $PEXE tmp.h5
  mv out/tmp.h5 ${FILE2}.h5
  rmdir out
  rm tmp.h5
elif [[ "$RESTART_OPT" == "1" ]] ; then
  # Option 1
  # start from input data alread "prepared" (already removed from invalid days)
#    ln -s ../oar.SHUJUAN.23191553/${FILE1}.h5 .  bug10hz 
#    ln -s ../oar.SHUJUAN.23191553/${FILE2}.h5 .
#    ln -s ../oar.SHUJUAN.23227113/${FILE1}.h5 .  bug10hz # with the incomplete 172 day
#    ln -s ../oar.SHUJUAN.23227113/${FILE2}.h5 .
#    ln -s ../oar.SHUJUAN.23246723_ref/${FILE1}.h5 .  # OK 20Hz avec err
#    ln -s ../oar.SHUJUAN.23246723_ref/${FILE2}.h5 .
# ref daily dataset:
  ln -s $INPUTDIR/${FILE1}.h5 .
  ln -s $INPUTDIR/${FILE2}.h5 .
elif [[ "$RESTART_OPT" == "2" ]] ; then
  # Option 2
  echo "Restart from existing correlation file"
  # Here we give the possibility to restart from an existing correlation file
  ## get a correlation file (if ln_corr = false)
  #ln -s /bettik/lecointre/oar.SHUJUAN.23272469_ref/corr_${FILE1}_${FILE2}.h5 . # ref avec nstep, ifnogap, n_valid_index
  #ln -s /bettik/lecointre/oar.SHUJUAN.23281081_ref/corr_${FILE1}_${FILE2}.h5 . # ref SP avec nstep, ifnogap, n_valid_index
  ln -s $INPUTDIR/corr_${FILE1}_${FILE2}.h5 . # ref DP calcul OK sur toutes les 3min de la journée
else
  "RESTART_DIR option : $RESTART_DIR not supported"
fi

END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Total elapsed time to get input: $DIFF sec"

################################################################################################

#case "$CLUSTER_NAME" in
#  luke)
#    module purge
#    module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2
#    ;;
#  ISTERRE)
#    module purge
#    module load fftw/3.3.4_intel-15.0.1 hdf5/1.10.2_serial_intel-18.0.1 
#    ;;
#esac

# prepare Cminv matrix (if ln_inversion=true)

if [ "$ln_inversion" == ".true." ] ; then

  echo "Compute Cminv:                            Date and Time: $(date +%F" "%T"."%N)"

  START=$(date +%s.%N)
  ./compute_Cminv
  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  echo "Total elapsed time to compute Cminv: $DIFF sec"

fi

##############################################################################

# Correlations - doublet - inversion

echo "Correlations-doublets-inversion:                            Date and Time: $(date +%F" "%T"."%N)"

START=$(date +%s.%N)
#export OMP_NUM_THREADS=$NBCORE
./$F90EXE
#/nfs_scratch/$USER/bin/time -f "\t%M Maximum resident set size (KB)" ./$F90EXE
# valgrind --leak-check=yes ./$F90EXE
END=$(date +%s.%N)
DIFF=$(echo "$END - $START" | bc)
echo "Total elapsed time to compute corr-doublets-inv: $DIFF sec"

###########################################################################

if [ "$ln_inversion" == ".true." ] ; then

  echo "Execute dvov_iworms.py code:                            Date and Time: $(date +%F" "%T"."%N)"

  PYV="python3"
  
  START=$(date +%s.%N)
  ln -s dtot_${FILE1}_${FILE2}.h5 in.h5
  ln -s dtmat_${FILE1}_${FILE2}.h5 inNWin.h5
  # ln -s corr_${FILE1}_${FILE2}.h5 corr.h5 # needed if ln_2D=0
  $PYV dvov_iworms.py --abs $ABS --autocorr $AUTOCORR
  mv out.h5 iwormsdvov_${FILE1}_${FILE2}.h5
  mv outNWin.h5 NWiniwormsdvov_${FILE1}_${FILE2}.h5
  rm in.h5 inNWin.h5
  END=$(date +%s.%N)
  DIFF=$(echo "$END - $START" | bc)
  echo "Total elapsed time to compute iworms dvov: $DIFF sec"

fi

#############################################################################

# prevent from corrupted file if not closed properly on bettik distributed file system (when data remains in cache while oar_job terminated with success)
sync

echo "End job:                                 Date and Time: $(date +%F" "%T"."%N)"
