!#---------------------------------
!$Id: corr_doublet_inversion_bypair.f90 249 2018-02-06 14:44:47Z lecointre $
!#---------------------------------

!###############################################################################
!#    This file is part of doublets projects : Fortran + python codes for      #
!#    computing correlations, measuring doublets, and invert it                #
!#                                                                             #
!#    Copyright (C) 2019 Albanne Lecointre, ...                                #
!#    albanne.lecointre@univ-grenoble-alpes.fr                                 #
!#                                                                             #
!#    This program is free software: you can redistribute it and/or modify     #
!#    it under the terms of the GNU General Public License as published by     #
!#    the Free Software Foundation, either version 3 of the License, or        #
!#    (at your option) any later version.                                      #
!#                                                                             #
!#    This program is distributed in the hope that it will be useful,          #
!#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
!#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
!#    GNU General Public License for more details.                             #
!#                                                                             #
!#    You should have received a copy of the GNU General Public License        #
!#    along with this program.  If not, see <https://www.gnu.org/licenses/>.   #
!###############################################################################

PROGRAM corr_doublet_inversion_bypair

  USE constants
  USE corrmodule
  USE doubletmodule
  USE inversionmodule
  USE HDF5 ! HDF5 module
  USE H5LT ! Lite Interface HDF5 : wrapper

#include "types.h"

  USE, intrinsic :: iso_c_binding

  IMPLICIT NONE

!  include 'fftw3.f03'

  ! Input namelist parameters
  INTEGER :: nn_datebeg,nn_dateend
  INTEGER :: nn_mcorr
  CHARACTER(LEN=256) :: cn_file1,cn_file2   ! Input file names (without .h5 suffix)
  INTEGER :: nn_samprate    ! Input dataset frequency
  INTEGER :: nn_unitseg     ! length in sec of a unitary segment (20min: 1200)
#ifdef lk_2Dinput
  INTEGER :: nn_nbunitseg   ! number of unitary segments in one hour (5) : this is the 2nd dim of 2D input dataset
#else
  INTEGER :: nn_overlap, nn_iter
  INTEGER :: jiter
#endif
  INTEGER :: nn_nextpow2,nn_dt_ref
  REAL(MYKIND) :: sn_lag        ! the lag in sec for the correlation
  LOGICAL :: ln_corr,ln_writecorr,ln_writedcorr,ln_doublets,ln_inversion,ln_writedtmat
  REAL(MYKIND) :: dist,Fmin,Fmax,LWin,distOverTmin,Tmax,Overlap,Fs  ! doublets
  INTEGER :: nn_pondcoef,nn_Lcorr   ! inversion
  REAL(KIND=4) :: sn_chunksizeparam

  LOGICAL :: l_exist_namelist
  LOGICAL :: l_dailymean

  INTEGER :: nph            ! the total number of samples in nn_unitseg segment (in each correlated segment)
  INTEGER :: maxlag         ! in nb of samples (sn_lag*60*nn_samprate)
  INTEGER :: zerolag
  INTEGER :: npc            ! = 2*maxlag+1 : total nb of points for the correlation
  INTEGER :: npad           ! nph+maxlag -> todo: option nextpow2
  INTEGER :: njday          ! Number of all jdays (valid or not) between datebeg and dateend
  INTEGER :: nvalidjday     ! The global number of valid jdays (typically <=1827)
  INTEGER :: ngit           ! ?   All time steps (without gaps) = nvalidjday*(24 if not l_dailymean)/nn_mcorr
  INTEGER :: npit           ! ?   Number of pair of time steps (=ngit*(ngit-1)/2)
  INTEGER :: nstep_ifnogap  ! ?   All time steps (without gaps) = nvalidjday*(24 if not l_dailymean)/nn_mcorr
  INTEGER :: nstep          ! ?   Number of valid time step after eventual decimation
  INTEGER, DIMENSION(2) :: nn
  INTEGER :: nstepm1        ! ?   Number of valid time step minus one to compute all the pairs of time steps

  CHARACTER(LEN=256) :: cn_prefixdir

  CHARACTER(LEN=2) :: chour
  CHARACTER(LEN=3) :: cjd
  CHARACTER(LEN=4) :: cyear
  CHARACTER(LEN=8) :: cP,cL,cN

  INTEGER, DIMENSION(:,:), ALLOCATABLE :: iarrtime,iarrtimetmp ! ?
  INTEGER, DIMENSION(:), ALLOCATABLE :: match,iarrvalid,iarrvalidtmp,infomean ! ?
  INTEGER, DIMENSION(:), ALLOCATABLE :: n_valid_index ! ?

  ! Parameters and Working array for correlations
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE, TARGET :: dcorr   ! = 2401,24*1827 = -1min,+1min , 24hours , 1827 days
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE, TARGET :: dcorr_ref   ! = 2401,24*1827 = -1min,+1min , 24hours , 1827 days
  !REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: dcorr   ! = 2401,24*1827 = -1min,+1min , 24hours , 1827 days
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rdcorr
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: rdcorr_ref
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: dcorrtmp   ! = 2401,24*1827 = -1min,+1min , 24hours , 1827 days
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: dcorrsum
  REAL(MYKIND) :: normfactor,corrnorm ! normfactor is to normalize corr : but it has no effect on dtot and dvov ?

  ! Parameters and Working array for doublets
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: FreqVec_large,FreqVec
  REAL(MYKIND), DIMENSION(:), POINTER :: CurrentFunction
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: WindowCurrent
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: dtmat
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: dtmat4
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: rdtmat4
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: clt
  REAL(MYKIND), DIMENSION(:,:,:), ALLOCATABLE :: dtmat_save
  REAL(KIND=4), DIMENSION(:,:,:), ALLOCATABLE :: rdtmat_save
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: WindowRefTot
  REAL(MYKIND) :: Tmin
  INTEGER :: nf
  INTEGER, DIMENSION(1) :: minlf,maxlf
  INTEGER :: NWin ! , DeltaWin
  REAL(MYKIND) :: DeltaWin
  REAL(MYKIND) :: invNWin,invNWinm1 ! (1/NWin) (1/(NWin-1)) 
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: tkw
  REAL(MYKIND) :: corrval
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: dtot
  REAL(KIND=4), DIMENSION(:,:), ALLOCATABLE :: rdtot
  INTEGER :: DeltaWinFs,LWinFs
  REAL(MYKIND), DIMENSION(:,:,:), ALLOCATABLE :: phase
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: phase_ref
  REAL(MYKIND), DIMENSION(:,:), ALLOCATABLE :: sxxall,axall
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: sxxall_ref,axall_ref
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: xlwork,ylwork
  INTEGER :: locidx
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: loc_idx

  ! Parameters and Working array for the inversion
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: MatCmP
  REAL(KIND=4), DIMENSION(1) :: misfit
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: AP,dvov
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: resolution,ApostError
  REAL(KIND=4), DIMENSION(:), ALLOCATABLE :: rdvov
  INTEGER :: jvh,nsomme,jdecim,indice

  ! Dummy loop iterators -> faire du nettoyage
  INTEGER   :: jhour         ! iterator on the hour
  INTEGER   :: jdecimhour         ! iterator on the hour
  INTEGER   :: js            ! iterator on the 20min unitary segment
  INTEGER   :: jt
  integer :: jstep,indicevalid,isum
  INTEGER :: idx,it1,it2,itnwin,ipc,ncount  ! dummy loop index
  REAL(MYKIND) :: t1 ! dummy loop index
  INTEGER :: jvd,jd,jh
  INTEGER :: ih,istep
  INTEGER :: ii

!$  integer(omp_lock_kind) :: omplock

  ! Working array and INFO for BLAS lapack routine
  REAL(MYKIND), DIMENSION(1) :: WORK ! working array for DGELS
  INTEGER :: INFO,LWORK

  INTEGER, DIMENSION(1) :: IWORK 

  ! FFT plans and Buffers for input and output FFT datasets
  INTEGER(KIND=8) :: plan_forward,plan_backward
  INTEGER :: plantype
  INTEGER :: rank
  INTEGER, DIMENSION(1) :: n_array
  COMPLEX(MYKINDCPLX), DIMENSION(:), ALLOCATABLE :: yout
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: din                    ! final result of IFFT (when manyplan=F)
  COMPLEX(MYKINDCPLX), DIMENSION(:), ALLOCATABLE :: youtpair
  COMPLEX(MYKINDCPLX), DIMENSION(:), ALLOCATABLE :: fftprod
  REAL(MYKIND), DIMENSION(:), ALLOCATABLE :: dinpad  ! all station traces but only one 20min chunk, padded with zeros before or after
  REAL(MYKIND),DIMENSION(:), ALLOCATABLE :: dind
  COMPLEX(MYKINDCPLX),DIMENSION(:), ALLOCATABLE :: you

  ! Manipulation des fichiers et des datasets HDF5
  ! Input
  INTEGER(HID_T), DIMENSION(2) :: infile_id      ! Input File identifier: this HDF5 file contains all the 1H (=5 x 20min) traces
  INTEGER(HSIZE_T), DIMENSION(1) :: indims       ! The hyperslab (memory) current dimension for input dataset : 20min at 20Hz = 24000
  ! Output
  INTEGER(HID_T) :: outfile_id     ! Output File identifier
  INTEGER(HSIZE_T), DIMENSION(2) :: outdims ! The total current dimension for output dataset
  INTEGER(HSIZE_T), DIMENSION(1) :: outdims1
  INTEGER(HSIZE_T), DIMENSION(3) :: outdims3
  INTEGER   :: error ! Error flag

  ! Instrumentation du code pour analyse de performance
  INTEGER,DIMENSION(8) :: values
  REAL(C_FLOAT) :: elps           ! in sec




  INTEGER :: nvhpd
  LOGICAL :: ln_naninf

  INTEGER(HID_T) :: dset_id, dataspace, memspace
  integer(hsize_t), dimension(2) :: offset, count
  integer(hsize_t), dimension(2)               :: dims ! ,maxdims
  integer(hsize_t), dimension(1) :: offset1, count1, dims1

  ! Declare NAMELIST
  NAMELIST /namtim/ nn_datebeg,nn_dateend,nn_mcorr 
  NAMELIST /nambp/ cn_file1,cn_file2
#ifdef lk_2Dinput
  NAMELIST /namparam2d/ ln_corr, cn_prefixdir, nn_samprate,nn_nextpow2,sn_lag,nn_unitseg,nn_nbunitseg,ln_writecorr,ln_writedcorr
#else
  NAMELIST /namparam/ ln_corr, cn_prefixdir, nn_samprate,nn_nextpow2,sn_lag,nn_unitseg,nn_overlap,nn_iter,ln_writecorr,ln_writedcorr
#endif
  NAMELIST /namdoublet/ ln_doublets,nn_dt_ref,dist,Fmin,Fmax,LWin,distOverTmin,Tmax,Overlap,Fs,ln_writedtmat
  NAMELIST /naminversion/ ln_inversion,nn_Lcorr,nn_pondcoef 
  NAMELIST /namscheduling/ sn_chunksizeparam 

  ! Read namelist
  INQUIRE(file='namelist', exist=l_exist_namelist)
  IF (.NOT. l_exist_namelist) THEN
     PRINT *,'The namelist does not exist'
     STOP
  ENDIF
  OPEN(11,file='namelist')
  PRINT *,'Reading namelist namtim'
    REWIND 11
    READ(11,namtim)
    PRINT *,'nn_datebeg: ',nn_datebeg
    PRINT *,'nn_dateend: ',nn_dateend
    IF (nn_mcorr .GE. 0) THEN
      l_dailymean = .false.
      PRINT *,'nn_mcorr: ',nn_mcorr,'-hour averaged correlations'
      IF ( (nn_mcorr /= 1) .AND. (nn_mcorr /= 2) &
                           .AND. (nn_mcorr /= 3) &
                           .AND. (nn_mcorr /= 4) &
                           .AND. (nn_mcorr /= 6) &
                           .AND. (nn_mcorr /= 8) &
                           .AND. (nn_mcorr /= 12)  ) THEN 
        print *,'valid values are 1,2,3,4,6,8,12 for hourly decimation: Abort ...'
        call EXIT(1)  ! Cigri sould abort
      ENDIF
    ELSE
      nn_mcorr = -nn_mcorr
      l_dailymean = .true.
      PRINT *,'nn_mcorr: ',nn_mcorr,'-day averaged correlations'
    ENDIF
  PRINT *,'Reading namelist nambp'
    REWIND 11
    READ(11,nambp)
    PRINT *,'cn_file1: ',TRIM(cn_file1),'.h5'
    PRINT *,'cn_file2: ',TRIM(cn_file2),'.h5'
  PRINT *,'Reading namelist namparam'
    REWIND 11
#ifdef lk_2Dinput
    READ(11,namparam2d)
#else
    READ(11,namparam)
#endif
    PRINT *,'ln_corr: ',ln_corr,': flag to compute corr or read them from a file' 
    PRINT *,'nn_samprate: ',nn_samprate,' Hz'
    PRINT *,'nn_unitseg: ',nn_unitseg,' sec'
#ifdef lk_2Dinput
    PRINT *,'nn_nbunitseg: ',nn_nbunitseg,' unitary segment(s)'
#else
    PRINT *,'nn_overlap: ',nn_overlap,' timestep duration (s)'
    PRINT *,'nn_iter: ',nn_iter,' number of timestep into one input dataset'
#endif
    PRINT *,'sn_lag: ',sn_lag,' sec'
    PRINT *,'nn_nextpow2: ',nn_nextpow2
    if (ln_corr) then
      PRINT *,'ln_writecorr: ',ln_writecorr,': flag to write correlations into a file'
      if (ln_writedcorr) print *,'as double precision'
    endif
  PRINT *,'Reading namelist namdoublet'
    REWIND 11
    READ(11,namdoublet)
    PRINT *,'ln_doublets: ',ln_doublets
    IF ( ln_doublets ) THEN
      IF ( nn_dt_ref .GE. 0 ) THEN
        PRINT *,'nn_dt_ref: ',nn_dt_ref,": calculating the doublet of each corr with regard to a reference"
        PRINT *,'... that implies forcing ln_inversion = F'
      ELSE
        PRINT *,'nn_dt_ref: ',nn_dt_ref,": calculating doublets between every two core and doing inversion"
      ENDIF
      PRINT *,'dist: ',dist
      PRINT *,'Fmin: ',Fmin
      PRINT *,'Fmax: ',Fmax
      PRINT *,'LWin: ',LWin
      IF (distOverTmin .gt. 0) THEN  ! strictly superior to 0
        PRINT *,'distOverTmin: ',distOverTmin
        Tmin = dist / distOverTmin
      ELSE
        Tmin = -distOverTmin
      ENDIF
      PRINT *,'Tmin: ',Tmin
      PRINT *,'Tmax: ',Tmax
      if ( sn_lag /= Tmax ) then
          print *,'Caution: Tmax is different from sn_lag'
        if ( ln_corr ) then
          print *,'sn_lag changed into ',Tmax
          sn_lag = Tmax
        else
          print *,'Assume that input corr file contains corr with sn_lag, and read it only with Tmax lag'
        endif
      endif
      PRINT *,'Overlap: ',Overlap
      PRINT *,'Fs: ',Fs
      PRINT *,'ln_writedtmat: ',ln_writedtmat,': flag to write dtmat into a file'
    ENDIF
  PRINT *,'Reading namelist naminversion'
    REWIND 11
    READ(11,naminversion)
    PRINT *,'ln_inversion: ',ln_inversion
    IF ( nn_dt_ref .GE. 0 ) THEN
      ln_inversion = .false.
      PRINT *,'nn_dt_ref: ',nn_dt_ref,": calculating the doublet of each corr with regard to a reference"
      PRINT *,'... that implies forcing ln_inversion = F'
    ENDIF
    IF ( ln_inversion ) THEN
      PRINT *,'nn_Lcorr: ',nn_Lcorr
      PRINT *,'nn_pondcoef: ',nn_pondcoef
    ENDIF
  PRINT *,'Reading namelist namscheduling'
    REWIND 11
    READ(11,namscheduling)
    PRINT *,'sn_chunksizeparam: ',sn_chunksizeparam

  CLOSE(11)

  CALL h5open_f(error)
  if (error /= 0) call exit(error)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!! make some useful computing about input time dimensions

  if (ln_corr) then

    ! Find njday (all days, valid or not) starting from datebeg and dateend
    njday = FIND_NJDAY(nn_datebeg,nn_dateend)

    ALLOCATE(iarrtime(2,njday))  ! Array (/ year , jday /) [njday]
    iarrtime = MAKE_DAYTIME_VECTOR(njday,nn_datebeg,nn_dateend)

    ! Open the 2 trace.h5 files
    CALL h5fopen_f(TRIM(cn_file1)//'.h5', H5F_ACC_RDONLY_F, infile_id(1), error)
    if (error /= 0) call exit(error)
    CALL h5fopen_f(TRIM(cn_file2)//'.h5', H5F_ACC_RDONLY_F, infile_id(2), error)
    if (error /= 0) call exit(error)

    ! find how many valid jday
    nvalidjday = FIND_VALID_NJDAY(njday,iarrtime,infile_id)

    ALLOCATE(iarrvalid(njday))  ! Array (/ 0;1 /) [njday] : valid jday or not
    iarrvalid = IS_VALID_JDAY(njday,iarrtime,infile_id)

    PRINT *,'njday',njday,'nvalidjday',nvalidjday

    ! Abort if nvalidjday = 0
    IF ( nvalidjday == 0 ) THEN
      print *,'nvalidjday:',nvalidjday,' Abort ...'
      call EXIT(0)  ! Cigri sould not abort
    ENDIF

  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! Compute some useful parameters
  maxlag = ceiling(sn_lag*nn_samprate)  ! as a number of samples : 6000
  npc = 2*maxlag+1             ! final length of the correlation vector : 2401

  rank = 1

  ! correlation lag time vector (in sec)
  ALLOCATE(clt(npc))
  clt = (/( ii * 1.0/nn_samprate , ii=-maxlag,maxlag )/)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! create fftw plans for computing correlations

! -> need to have npad
  if (ln_corr) then

    ! Compute some useful parameters
    nph = nn_unitseg*nn_samprate ! length of a unitary 20min segment : 24000
    IF ( nn_nextpow2 == 0 ) THEN
      npad = ceiling((nn_unitseg+sn_lag) * nn_samprate)
    ELSE
      npad = nn_nextpow2
    ENDIF
 
    ! Create the 2 plans for FFT forward and FFT backward
    plantype=1
    call compute_fftw_plan(plan_forward, npad, plantype)
    plantype=-1
    call compute_fftw_plan(plan_backward, npad, plantype)

  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!! CORRELATIONS !!!
! Computes correlations (hourly or daily mean)

  if (ln_corr) then

    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)

    ALLOCATE(youtpair(npad/2+1),din(npad))
    ALLOCATE(fftprod(npad/2+1))
    nstep = nvalidjday
    nstep_ifnogap = njday

#ifdef lk_2Dinput
! here the input dataset is 2D hourly dataset : overlapping is managed by this 2D...

    IF (l_dailymean) THEN
      ALLOCATE(dcorr(npc,nstep))
      jvd = 0 ! iterator over valid day
      DO jd = 1,njday
        IF ( iarrvalid(jd) .EQ. 1 ) THEN
          nvhpd = 24*nn_nbunitseg ! normal nb of hours in a day
          jvd=jvd+1
          write(cyear,'(I0.4)') iarrtime(1,jd)
          write(cjd,'(I0.3)') iarrtime(2,jd)
          youtpair(:)=(ZERO,ZERO)
          DO jh = 1,24
            write(chour,'(I0.2)') jh-1
            ! Loop on the 5 x 20min segments
            DO js=1, nn_nbunitseg
              !print *,'fftprod jh js',jh,js
              ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
              CALL COMPUTE_FFTPROD(nph,js,maxlag,npad,infile_id,cyear,cjd,chour,plan_forward,fftprod,ln_naninf)
              IF (ln_naninf) THEN
                nvhpd = nvhpd-1 ! decrease number of valid timesteps in a day
                print *,'invalid timesteps: ',js, jh, cjd, cyear
              ELSE
                youtpair(:) = youtpair(:)+fftprod(:)
              ENDIF
            ENDDO ! End loop on js
          ENDDO ! End loop on jh
#ifdef lk_float
          call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
          call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
          if (nvhpd < 24*nn_nbunitseg ) print *,'weak nvhpd: ',nvhpd, cjd, cyear
          corrnorm = (UN / nvhpd) / npad
          dcorr(:,jvd) = corrnorm * din(1:2*maxlag+1)
        ENDIF
      ENDDO
    ELSE
      corrnorm = (UN / nn_nbunitseg) / npad
      nstep = 24*nstep
      nstep_ifnogap = 24*nstep_ifnogap
      ALLOCATE(dcorr(npc,nstep))
      jvh = 0 ! iterator over valid jhour
      DO jt = 1,24*njday
        IF ( iarrvalid(1+(jt-1)/24) .EQ. 1 ) THEN
          jvh=jvh+1
          write(cyear,'(I0.4)') iarrtime(1,1+(jt-1)/24)
          write(cjd,'(I0.3)') iarrtime(2,1+(jt-1)/24)
          write(chour,'(I0.2)') MOD(jt-1,24)
          youtpair(:)=(ZERO,ZERO)
          DO js = 1, nn_nbunitseg
            ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
            CALL COMPUTE_FFTPROD(nph,js,maxlag,npad,infile_id,cyear,cjd,chour,plan_forward,fftprod,ln_naninf)
            ! here we do not take care about ln_naninf.... -> todo
            youtpair(:) = youtpair(:)+fftprod(:)
          ENDDO ! End loop on js
#ifdef lk_float
          call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
          call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
          dcorr(:,jvh) = corrnorm * din(1:2*maxlag+1)
        ENDIF
      ENDDO
    ENDIF

#else
! here the input dataset is daily dataset (or whatever we want), but it is 1D dataset

    IF (l_dailymean) THEN
      ALLOCATE(dcorr(npc,nstep))
      jvd = 0 ! iterator over valid day
      DO jd = 1,njday
        IF ( iarrvalid(jd) .EQ. 1 ) THEN
          nvhpd = nn_iter ! normal nb of timesteps in a day
          jvd=jvd+1
          write(cyear,'(I0.4)') iarrtime(1,jd)
          write(cjd,'(I0.3)') iarrtime(2,jd)
          youtpair(:)=(ZERO,ZERO)
          DO jiter = 1, nn_iter  ! nb of iteration into one 1D-inputdataset
            js = (jiter-1)*nn_overlap*nn_samprate   ! offset in 1D-inputdata
!              print *,'fftprod jh js',jh,js
            ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
            CALL COMPUTE_FFTPROD(nph,js,maxlag,npad,infile_id,cyear,cjd,plan_forward,fftprod,ln_naninf)
            IF (ln_naninf) THEN
              nvhpd = nvhpd-1 ! decrease number of valid timesteps in a day
              print *,'invalid timesteps: ',jiter, js, cjd, cyear
            ELSE
              youtpair(:) = youtpair(:)+fftprod(:)
            ENDIF
          ENDDO ! End loop on js
#ifdef lk_float
          call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
          call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
          if (nvhpd < nn_iter ) print *,'weak nvhpd: ',nvhpd, cjd, cyear
          corrnorm = (UN / nvhpd) / npad
          dcorr(:,jvd) = corrnorm * din(1:2*maxlag+1)
        ENDIF
      ENDDO ! end loop on jd
    ELSE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      nstep = 24*nstep                 ! nb of valid hours
      nstep_ifnogap = 24*nstep_ifnogap ! nb of total hours
      ALLOCATE(dcorr(npc,nstep))
      jvh = 0 ! iterator over valid jhour
      DO jt = 1,24*njday
        IF ( iarrvalid(1+(jt-1)/24) .EQ. 1 ) THEN
          nvhpd = nn_iter ! normal nb of timesteps in one hour
          jvh=jvh+1
          write(cyear,'(I0.4)') iarrtime(1,1+(jt-1)/24)
          write(cjd,'(I0.3)') iarrtime(2,1+(jt-1)/24)
          write(chour,'(I0.2)') MOD(jt-1,24)
!          print *,cyear,cjd,chour
          youtpair(:)=(ZERO,ZERO)
          DO jiter = 1, nn_iter ! ! nb of iteration into one 1h subset from 1D daily dataset
            js = MOD(jt-1,24)*3600*nn_samprate  + (jiter-1)*nn_overlap*nn_samprate   ! offset in 1D-inputdata
!            print *,js
            ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
            CALL COMPUTE_FFTPROD(nph,js,maxlag,npad,infile_id,cyear,cjd,plan_forward,fftprod,ln_naninf)
            IF (ln_naninf) THEN
              nvhpd = nvhpd-1 ! decrease number of valid timesteps in a day
              print *,'invalid timesteps: ',jiter, js, cjd, cyear, chour
            ELSE
              youtpair(:) = youtpair(:)+fftprod(:)
            ENDIF
          ENDDO ! End loop on jiter
#ifdef lk_float
          call sfftw_execute_dft_c2r(plan_backward,youtpair,din)
#else
          call dfftw_execute_dft_c2r(plan_backward,youtpair,din)
#endif
          if (nvhpd < nn_iter ) print *,'weak nvhpd: ',nvhpd, cjd, cyear, chour
          corrnorm = (UN / nvhpd) / npad
          dcorr(:,jvh) = corrnorm * din(1:2*maxlag+1)
        ENDIF
      ENDDO ! end loop on jt = jhour
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ENDIF

#endif

    DEALLOCATE(youtpair,fftprod,din)
    CALL h5fclose_f(infile_id(1), error)
    if (error /= 0) call exit(error)
    CALL h5fclose_f(infile_id(2), error)
    if (error /= 0) call exit(error)
#ifdef lk_float
    CALL sfftw_destroy_plan(plan_forward)
    CALL sfftw_destroy_plan(plan_backward)
#else
    CALL dfftw_destroy_plan(plan_forward)
    CALL dfftw_destroy_plan(plan_backward)
#endif

    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elps time to cpt "I5" corr for "I5" valid day(s) over "I5" days : ",f20.6," s")', &
           nstep,nvalidjday,njday,elps
 
  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!! DECIMATION OVER CORRELATIONS !!!

  if (ln_corr) then
 
! Decimate hourly or daily computed correlations
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
    IF ( nn_mcorr /= 1 ) THEN
  
      nstep_ifnogap = nstep_ifnogap / nn_mcorr
  
      IF (l_dailymean) THEN
      ! Here we give the possibility to decrease correlation frequency over several days
      ! it works even if some days are missing thanks to iarrvalid
  
      ! allocate decimated arrays (containing all decimated steps
        ALLOCATE(iarrtimetmp(2,nstep_ifnogap),iarrvalidtmp(nstep_ifnogap))
        ! Find the new dcorr size (ca tronque a la fin si besoin...)
        nstep = 0
        indice = 0
        DO jt = 1, nstep_ifnogap  ! parcourt les groupes de jours
          nsomme = 0
          DO jdecim = 1, nn_mcorr  ! parcourt chaque jour du groupe de jour
             indice = indice + 1
             nsomme = nsomme + iarrvalid(indice)
          ENDDO
          iarrtimetmp(:,jt) = iarrtime(:,indice)
          if ( nsomme .GT. 0 ) then
            nstep = nstep+1
            iarrvalidtmp(jt) = 1
          else
            iarrvalidtmp(jt) = 0
          endif
        ENDDO
        ! allocate decimated arrays (containing only valid decimated steps)
        ALLOCATE(dcorrtmp(npc,nstep),dcorrsum(npc),infomean(nstep))
        dcorrtmp(:,:) = ZERO
        ! fill decimated arrays
        jstep = 0
        indice = 0
        indicevalid=0
        DO jt = 1, nstep_ifnogap  ! parcourt les groupes de jours
          dcorrsum(:)=ZERO
          isum=0
          DO jdecim = 1,nn_mcorr
            indice = indice + 1
            ! is dcorr valid ?
            IF ( iarrvalid(indice) == 1 ) THEN
              indicevalid = indicevalid + 1
              isum=isum+1
              dcorrsum(:) = dcorrsum(:)+dcorr(:,indicevalid)
            ENDIF
          ENDDO
          IF ( iarrvalidtmp(jt) == 1 ) THEN
            jstep = jstep+1
            dcorrtmp(:,jstep) = dcorrsum(:)/isum
            infomean(jstep) = isum
          ENDIF
        ENDDO
        DEALLOCATE(dcorr,iarrtime,iarrvalid)
        ALLOCATE(dcorr(npc,nstep),iarrtime(2,nstep_ifnogap),iarrvalid(nstep_ifnogap))
        dcorr(:,:)=dcorrtmp(:,:)
        iarrtime(:,:) = iarrtimetmp(:,:)
        iarrvalid(:) = iarrvalidtmp(:)
        DEALLOCATE(dcorrtmp,iarrtimetmp,iarrvalidtmp)
    
        ! create the vector of valid index
        ALLOCATE(n_valid_index(nstep))
        indice=0
        DO jt=1,nstep_ifnogap
          IF ( iarrvalid(jt) == 1 ) THEN
            indice=indice+1
            n_valid_index(indice)=jt
          ENDIF
        ENDDO
    
      ELSE
    
! Here we give the possibility to decrease hourly correlation frequency over several hours
        nstep=nstep/nn_mcorr
        ALLOCATE(dcorrtmp(npc,nstep))
        dcorrtmp(:,:)=ZERO
        jdecimhour=1
        DO jhour=1,nstep*nn_mcorr
          dcorrtmp(:,jdecimhour)=dcorrtmp(:,jdecimhour)+dcorr(:,jhour)/nn_mcorr
          IF ( MOD(jhour,nn_mcorr) == 0 ) THEN
            jdecimhour=jdecimhour+1
          ENDIF
        ENDDO
        DEALLOCATE(dcorr)
        ALLOCATE(dcorr(npc,nstep))
        dcorr(:,:)=dcorrtmp(:,:)
        DEALLOCATE(dcorrtmp)
      
        ! create the vector of valid index
        ALLOCATE(n_valid_index(nstep))
        indice = 0
        istep = 0
        DO jt=1,njday
          IF ( iarrvalid(jt) == 1 ) THEN
            DO ih = 1, 24/nn_mcorr  ! combien de paquets de n h dans une journee
              indice = indice+1
              istep=istep+1
              n_valid_index(indice)=istep
            ENDDO
          ELSE
            DO ih = 1, 24/nn_mcorr
              istep=istep+1
            ENDDO
          ENDIF
        ENDDO
    
      ENDIF
  
    ELSE ! no decimation at all
  
      IF (l_dailymean) THEN
  
        ! create the vector of valid index
        ALLOCATE(n_valid_index(nstep))
        indice = 0
        istep = 0
        DO jt=1,njday
          IF ( iarrvalid(jt) == 1 ) THEN
            indice = indice+1
            istep=istep+1
            n_valid_index(indice)=istep
          ELSE
            istep=istep+1
          ENDIF
        ENDDO
  
      ELSE
  
        ! create the vector of valid index
        ALLOCATE(n_valid_index(nstep))
        indice = 0
        istep = 0
        DO jt=1,njday
          IF ( iarrvalid(jt) == 1 ) THEN
            DO ih = 1, 24  ! combien de paquets de n h dans une journee
              indice = indice+1
              istep=istep+1
              n_valid_index(indice)=istep
            ENDDO
          ELSE
            DO ih = 1, 24
              istep=istep+1
            ENDDO
          ENDIF
        ENDDO
  
      ENDIF
  
    ENDIF
      
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elps time to decimate corr: "I5" timesteps from "I5" valid day(s) over "I5" days : ",f20.6," s")', &
           nstep,nvalidjday,njday,elps
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  ! normalisation des green functions
  !! je remark que ca n'a pas d'impact sur le dtot (delta~1e-8)
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
    DO jt = 1,nstep
      normfactor = UN / MAXVAL(ABS(dcorr(:,jt)))
      dcorr(:,jt) = dcorr(:,jt) * normfactor
    ENDDO
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elapsed time to normalize correlations: "I5" timesteps : ",f20.6," s")',nstep,elps
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  ! compute the average of correlations

    if ( nn_dt_ref .GE. 0 ) then
      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
      ALLOCATE(dcorr_ref(npc))
      if ( nn_dt_ref .EQ. 0 ) then
        dcorr_ref(:) = ZERO
        DO it1=1,nstep
          dcorr_ref(:) = dcorr_ref(:) + ( dcorr(:,it1) / nstep )
        ENDDO
      else
        IWORK = MINLOC(ABS(n_valid_index(:)-nn_dt_ref))
        print *,"Assuming time reference is ",nn_dt_ref
        print *,"find the nearest one: n_valid_index(",iwork(1),")=",n_valid_index(iwork(1))
        dcorr_ref(:) = dcorr(:,iwork(1)) 
      endif
      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
      PRINT '("Total elapsed time to average correlations: "I5" timesteps : ",f20.6," s")',nstep,elps
    endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    IF ( ln_writecorr ) THEN
  ! write correlations
      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
      CALL h5fcreate_f('corr_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
      if (error /= 0) call exit(error)
      outdims1=(/npc/)
      outdims=(/npc,nstep/)
#ifdef lk_float
      CALL h5ltmake_dataset_float_f(outfile_id, '/scorr', 2, outdims, dcorr(:,:), error)
      if (error /= 0) call exit(error)
      if (nn_dt_ref .GE. 0) then
        CALL h5ltmake_dataset_float_f(outfile_id, '/scorr_ref', 1, outdims1, dcorr_ref(:), error)
        if (error /= 0) call exit(error)
      endif
#else
      IF ( ln_writedcorr ) THEN
        CALL h5ltmake_dataset_double_f(outfile_id, '/dcorr', 2, outdims, dcorr(:,:), error)
        if (error /= 0) call exit(error)
        if (nn_dt_ref .GE. 0) then
          CALL h5ltmake_dataset_double_f(outfile_id, '/dcorr_ref', 1, outdims1, dcorr_ref(:), error)
          if (error /= 0) call exit(error)
        endif
      ELSE
        ALLOCATE(rdcorr(npc,nstep))
        rdcorr(:,:) = REAL(dcorr(:,:))
        CALL h5ltmake_dataset_float_f(outfile_id, '/scorr', 2, outdims, rdcorr(:,:), error)
        if (error /= 0) call exit(error)
        DEALLOCATE(rdcorr)
        if (nn_dt_ref .GE. 0) then
          ALLOCATE(rdcorr_ref(npc))
          rdcorr_ref(:) = REAL(dcorr_ref(:))
          CALL h5ltmake_dataset_float_f(outfile_id, '/scorr_ref', 1, outdims1, rdcorr_ref(:), error)
          if (error /= 0) call exit(error)
          DEALLOCATE(rdcorr_ref)
        endif
      ENDIF
#endif
      IF (l_dailymean) THEN 
        outdims=(/2,nstep_ifnogap/)
        outdims1=(/nstep_ifnogap/)
      ELSE
        outdims=(/2,njday/)
        outdims1=(/njday/)
      ENDIF
      CALL h5ltmake_dataset_int_f(outfile_id, '/iarrtime', 2, outdims, iarrtime(:,:), error)
      if (error /= 0) call exit(error)
      CALL h5ltmake_dataset_int_f(outfile_id, '/iarrvalid', 1, outdims1, iarrvalid(:), error)
      if (error /= 0) call exit(error)
      outdims1=(/nstep/)
      CALL h5ltmake_dataset_int_f(outfile_id, '/n_valid_index', 1, outdims1, n_valid_index(:), error)
      if (error /= 0) call exit(error)
      outdims1=(/npc/)
      CALL h5ltmake_dataset_float_f(outfile_id, '/clt', 1, outdims1, clt(:), error)
      if (error /= 0) call exit(error)
      outdims1=(/2/)
      nn(1) = nstep
      nn(2) = nstep_ifnogap
      CALL h5ltmake_dataset_int_f(outfile_id, '/nstep', 1, outdims1, nn, error)
      if (error /= 0) call exit(error)
      CALL h5fclose_f(outfile_id, error)
      if (error /= 0) call exit(error)
      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
      PRINT '("Total elapsed time to write correlations: "I5" timesteps : ",f20.6," s")',nstep,elps
    ENDIF
  
    IF (l_dailymean .AND. (nn_mcorr /= 1)) THEN 
      CALL h5fcreate_f('infomean_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
      if (error /= 0) call exit(error)
      outdims1=(/nstep/)
      CALL h5ltmake_dataset_int_f(outfile_id, '/infomean', 1, outdims1, infomean(:), error)
      if (error /= 0) call exit(error)
      CALL h5fclose_f(outfile_id, error)
      if (error /= 0) call exit(error)
    ENDIF
  
  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if (.not. ln_corr) then
    ! read corr from existing input file
    ! sn_lag should be the lag into input file
    ! use Tmax and sn_lag for determining offset for reading only a subset from correlation input file

    CALL h5fopen_f('corr_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_RDONLY_F, infile_id(1), error)
    if (error /= 0) call exit(error)

!! read nstep et nstepifnogap

    call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/nstep", dset_id, error)
    if (error /= 0) call exit(error)
    outdims1 = (/2/)
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, nn, outdims1, error)
    if (error /= 0) call exit(error)
    nstep=nn(1)
    nstep_ifnogap=nn(2)
    CALL h5dclose_f(dset_id, error)
    if (error /= 0) call exit(error)
    call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/n_valid_index", dset_id, error)
    if (error /= 0) call exit(error)
    outdims1=(/nstep/)
    allocate(n_valid_index(nstep))
    call h5dread_f(dset_id, H5T_NATIVE_INTEGER, n_valid_index, outdims1, error)
    if (error /= 0) call exit(error)
    CALL h5dclose_f(dset_id, error)
    if (error /= 0) call exit(error)

    ! 0based index of 0lag in the case of sn_lag corr
    zerolag = maxlag ! (with the previous maxlag computed with sn_lag)

    maxlag = ceiling(Tmax*nn_samprate)  ! as a number of samples : 6000
    npc = 2*maxlag+1             ! what we are really using from possible larger correlation

    allocate(dcorr(npc, nstep))
    dims = (/npc, nstep/)
    count = (/npc, nstep/)
    offset = (/zerolag-maxlag, 0/)

print *,'Reading correlation with Tmax lag'
print *,'offset',offset
print *,'count',count

#ifdef lk_float
    call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/scorr", dset_id, error)
#else
! the dataset MUST be double prec
    call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/dcorr", dset_id, error)
#endif
    if (error /= 0) call exit(error)

    ! Get dataset's dataspace identifier and select subset.
    CALL h5dget_space_f(dset_id, dataspace, error)
    if (error /= 0) call exit(error)
    CALL h5sselect_hyperslab_f(dataspace, H5S_SELECT_SET_F, offset, count, error)
    if (error /= 0) call exit(error)

    CALL h5screate_simple_f(2, dims, memspace, error)
    if (error /= 0) call exit(error)

#ifdef lk_float
    CALL h5dread_f(dset_id, H5T_NATIVE_REAL, dcorr, dims, error, memspace, dataspace)    
#else
    CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dcorr, dims, error, memspace, dataspace)    
#endif
    if (error /= 0) call exit(error)

    CALL h5sclose_f(memspace, error)
    if (error /= 0) call exit(error)
    CALL h5sclose_f(dataspace, error)
    if (error /= 0) call exit(error)
    CALL h5dclose_f(dset_id, error)
    if (error /= 0) call exit(error)

    if ( nn_dt_ref .GE. 0 ) then

      allocate(dcorr_ref(npc))
      dims1 = (/npc/)
      count1 = (/npc/)
      offset1 = (/zerolag-maxlag/)

#ifdef lk_float
      call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/scorr_ref", dset_id, error)
#else
! the dataset MUST be double prec
      call h5dopen_f(infile_id(1), "/"//TRIM(cn_prefixdir)//"/dcorr_ref", dset_id, error)
#endif
      if (error /= 0) call exit(error)

      ! Get dataset's dataspace identifier and select subset.
      CALL h5dget_space_f(dset_id, dataspace, error)
      if (error /= 0) call exit(error)
      CALL h5sselect_hyperslab_f(dataspace, H5S_SELECT_SET_F, offset1, count1, error)
      if (error /= 0) call exit(error)

      CALL h5screate_simple_f(1, dims1, memspace, error)
      if (error /= 0) call exit(error)

#ifdef lk_float
      CALL h5dread_f(dset_id, H5T_NATIVE_REAL, dcorr_ref, dims1, error, memspace, dataspace)    
#else
      CALL h5dread_f(dset_id, H5T_NATIVE_DOUBLE, dcorr_ref, dims1, error, memspace, dataspace)    
#endif
      if (error /= 0) call exit(error)

      CALL h5sclose_f(memspace, error)
      if (error /= 0) call exit(error)
      CALL h5sclose_f(dataspace, error)
      if (error /= 0) call exit(error)
      CALL h5dclose_f(dset_id, error)
      if (error /= 0) call exit(error)

    endif

    CALL h5fclose_f(infile_id(1), error)
    if (error /= 0) call exit(error)

  endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  if ( ln_doublets ) then 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Now we compute the doublets

    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  
    !% Define Frequency vector and cut between Fmin and Fmax
    ALLOCATE(FreqVec_large(npc))
    DO ipc=1,npc
      FreqVec_large(ipc)=(ipc-1)*Fs/(npc-1)
    ENDDO
    minlf=MINLOC(FreqVec_large,mask = FreqVec_large > Fmin)
    maxlf=MAXLOC(FreqVec_large,mask = FreqVec_large < Fmax)
    nf=maxlf(1)-minlf(1)+1
    ALLOCATE(FreqVec(nf))
    FreqVec(:)=FreqVec_large(minlf(1):maxlf(1))
    DEALLOCATE(FreqVec_large)
 
    DeltaWin=LWin*(UN-Overlap/100.0) !  time difference between consecutive windows (real)
    NWin=ceiling((Tmax-Tmin)/DeltaWin)*2 ! nb of windows
    LWinFs=nint(LWin*Fs) ! nb of samples in one window
    DeltaWinFs=nint(DeltaWin*Fs) ! nb of samples from one window to another
  
    ALLOCATE(tkw(LWinFs))
    CALL tukeywin(tkw,LWinFs,ZEROCINQ)
  
    ! Create the forward plan to compute dt
    n_array(1) = npc
    ALLOCATE(dind(npc),you(npc/2+1))
    dind(:)=ZERO
    you(:)=(ZERO,ZERO)
#ifdef lk_float
    CALL sfftw_plan_dft_r2c(plan_forward, rank, n_array, &
                                 dind,you,FFTW_ESTIMATE)
#else
    CALL dfftw_plan_dft_r2c(plan_forward, rank, n_array, &
                                 dind,you,FFTW_ESTIMATE)
#endif
    DEALLOCATE(dind,you)
  
  !-------------------------------------------------------------
  ! First we compute and store all the phase from FFT+1 (1.75GB if simple precision)
  
    ALLOCATE(loc_idx(2,NWin))
    ALLOCATE(dtmat4(NWin))  ! 1dim=dt, 2dim=err, 3dim=coherence, 4dim=time
    loc_idx(1,1)=1
    loc_idx(2,1)=LWinFs
    dtmat4(1)=-Tmax+LWin/2
    t1=-Tmax+DeltaWin   ! time of the beginning of the window  -60+2 = -58s
    ncount=2
    DO WHILE (t1 < -Tmin)
      locidx=DeltaWinFs*(ncount-1)
      loc_idx(1,ncount)=1+locidx
      loc_idx(2,ncount)=LWinFs+locidx
      dtmat4(ncount)=t1+LWin/2
      t1=t1+DeltaWin
      ncount=ncount+1
    ENDDO
    loc_idx(1,NWin)=npc-LWinFs+1
    loc_idx(2,NWin)=npc
    dtmat4(NWin)=Tmax-LWin/2
    t1=Tmax-DeltaWin    ! time of the beginning of the window : +58sec
    ncount=1
    DO WHILE (t1 > Tmin)
      idx=NWin-ncount
      locidx=npc-nint((LWin+DeltaWin*ncount)*Fs)
      loc_idx(1,idx)=1+locidx
      loc_idx(2,idx)=LWinFs+locidx
      dtmat4(idx)=t1-LWin/2
      t1=t1-DeltaWin
      ncount=ncount+1
    ENDDO

print *,'iwin,dtmat4,loc_idx'
DO ncount = 1, NWin
  print *,ncount, dtmat4(ncount),loc_idx(:,ncount)
enddo
    if ( ln_writedtmat ) THEN  
      CALL h5fcreate_f('dtmat_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
      if (error /= 0) call exit(error)
      outdims1 = (/NWin/)
      ALLOCATE(rdtmat4(NWin))
      rdtmat4(:) = REAL(dtmat4(:))
      CALL h5ltmake_dataset_float_f(outfile_id, '/dtmat4', 1, outdims1, rdtmat4(:), error)
      if (error /= 0) call exit(error)
      outdims = (/2, NWin/)
      CALL h5ltmake_dataset_int_f(outfile_id, '/loc_idx', 2, outdims, loc_idx(:,:), error)
      if (error /= 0) call exit(error)
      DEALLOCATE(rdtmat4)
    endif


    ALLOCATE(WindowCurrent(npc))

    ALLOCATE(phase(nf,NWin,nstep))  ! 89 , 60 , 43848
    ALLOCATE(sxxall(NWin,nstep),axall(NWin,nstep))  ! 60,43848
  
    DO it1=1,nstep
  
      CurrentFunction => dcorr(:,it1)
  
      WindowCurrent(:)=ZERO
      DO itnwin=1,NWin
        CALL applyTKW_noinit(npc,CurrentFunction,loc_idx(1,itnwin),loc_idx(2,itnwin),tkw,WindowCurrent)
        CALL prepare_pearsn(npc,LWinFs,WindowCurrent(loc_idx(1,itnwin):loc_idx(2,itnwin)),axall(itnwin,it1),sxxall(itnwin,it1))
        CALL computeFFT(npc,WindowCurrent,plan_forward,minlf(1),nf,phase(:,itnwin,it1))  ! element 1  : -60s : 1 iteration
        WindowCurrent(loc_idx(1,itnwin):loc_idx(2,itnwin))=ZERO
      ENDDO
  
    ENDDO  

    if ( nn_dt_ref .GE. 0 ) then

      ALLOCATE(phase_ref(nf,NWin))
      ALLOCATE(sxxall_ref(NWin),axall_ref(NWin))  ! 60,43848
      CurrentFunction => dcorr_ref(:)
      WindowCurrent(:)=ZERO
      DO itnwin=1,NWin
        CALL applyTKW_noinit(npc,CurrentFunction,loc_idx(1,itnwin),loc_idx(2,itnwin),tkw,WindowCurrent)
        CALL prepare_pearsn(npc,LWinFs,WindowCurrent(loc_idx(1,itnwin):loc_idx(2,itnwin)),axall_ref(itnwin),sxxall_ref(itnwin))
        CALL computeFFT(npc,WindowCurrent,plan_forward,minlf(1),nf,phase_ref(:,itnwin))  ! element 1  : -60s : 1 iteration
        WindowCurrent(loc_idx(1,itnwin):loc_idx(2,itnwin))=ZERO
      ENDDO

    endif
 
    DEALLOCATE(WindowCurrent)
  
#ifdef lk_float
    CALL sfftw_destroy_plan(plan_forward)
#else
    CALL dfftw_destroy_plan(plan_forward)
#endif
  
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elapsed time to compute "I3" x "I5" ctimes FFT+1 : ",f20.6," s")',NWin,nstep,elps
  
  
  ! And now we compute corrval and doublets
  
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  
    if ( nn_dt_ref .GE. 0 ) then
      ALLOCATE(match(nstep))
      match(:) = n_valid_index(:)
    else
      ALLOCATE(match(nstep*(nstep-1)/2))
      CALL map_pair(nstep_ifnogap,nstep,n_valid_index,match)
    endif

    ngit=nstep_ifnogap ! nn_njday*24/nn_mcorr
    npit = ngit*(ngit-1)/2
    ! pour l'inversion apr�s il vaut mieux que ce soit la dim npit contigue en memoire
    if ( nn_dt_ref .GE. 0 ) then
      ALLOCATE(dtot(ngit,3))
      outdims=(/ngit,3/)
    else
      ALLOCATE(dtot(npit,3))
      outdims=(/npit,3/)
    endif
    dtot(:,:)=ZERO
  ! remark: on pourrait n'init a zero que les trous
  
  !  CALL date_and_time(VALUES=values)
  !  elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  
  ! Compute the optimal LWORK for xGELS
    ALLOCATE(xlwork(nf),ylwork(nf))
    !CALL DGELS( TRANS, M, N, NRHS, A, LDA, B, LDB, WORK, LWORK, INFO )
#ifdef lk_float
    CALL SGELS( 'N', nf, 1, 1, xlwork, nf, ylwork, nf, WORK, -1, INFO )
#else
    CALL DGELS( 'N', nf, 1, 1, xlwork, nf, ylwork, nf, WORK, -1, INFO )
#endif
    IF (INFO==0) THEN
      LWORK=WORK(1)
      print *,'xGELS Computing optimal size for WORK, LWORK=',LWORK
    ELSE
      print *,'xGELS Computing optimal size for WORK, INFO=', INFO,' Abort'
      CALL EXIT(1)  ! Cigri must abort
    ENDIF
  
    invNWin=UN/NWin
    invNWinm1=UN/(NWin-1)
    nstepm1=nstep-1
  
  ! Ici les differentes taches ne sont pas du tout equilibrees (on remplit un triangle) : SCHEDULE(RUNTIME) :
  !  - soit DYNAMIC avec un petit paquet de taille donnee (plus petit que nb_tot_iteration/nb_threads, environ 10% serait bien)
  !  - soit GUIDED (taille du paquet decroit exponentiellement)
  !  - en fait il faudrait parcourir la boucle a l'envers, pour commencer par les iterations "rapides"
  
    ALLOCATE(WindowRefTot(LWinFs,NWin))
    ALLOCATE(WindowCurrent(LWinFs))
    ALLOCATE(dtmat(NWin,3))  ! 1dim=dt, 2dim=err, 3dim=coherence, 4dim=time
    if ( ln_writedtmat ) THEN 
      if ( nn_dt_ref .GE. 0 ) then 
        ALLOCATE(dtmat_save(NWin,3,nstep)) 
      else
        ALLOCATE(dtmat_save(NWin,3,nstep*(nstep-1)/2)) 
      endif
    endif

    IF ( nn_dt_ref .GE. 0 ) THEN

!!! calculating the doublet of each corr with regard to a reference (the reference can be defined as the average of all corrs)

      ! WindowRef
      DO itnwin=1,NWin
        CurrentFunction => dcorr_ref(loc_idx(1,itnwin):loc_idx(2,itnwin))
        CALL applyTKW_noinit_nopad(CurrentFunction,LWinFs,tkw,WindowRefTot(:,itnwin))
      ENDDO

      DO it1=1,nstep
        ! WindowCurrent
        ! compute doublet

        corrval=ZERO
        DO itnwin=1,NWin
          CurrentFunction => dcorr(loc_idx(1,itnwin):loc_idx(2,itnwin),it1)
          CALL applyTKW_noinit_nopad(CurrentFunction,LWinFs,tkw,WindowCurrent)
          CALL measureDt(npc,LWinFs,nf,LWORK, &
                         FreqVec,phase_ref(:,itnwin),phase(:,itnwin,it1), &
                         axall_ref(itnwin),axall(itnwin,it1),sxxall_ref(itnwin),sxxall(itnwin,it1), &
                         WindowRefTot(:,itnwin),WindowCurrent, &
                         dtmat(itnwin,1),dtmat(itnwin,2),dtmat(itnwin,3)) ! dt, dterr, corrval
          corrval=corrval+dtmat(itnwin,3)
        ENDDO
        if ( ln_writedtmat ) THEN  
          dtmat_save(:,1,it1) = dtmat(:,1)
          dtmat_save(:,2,it1) = dtmat(:,2)
          dtmat_save(:,3,it1) = dtmat(:,3)
        endif
        corrval=corrval*invNWin
        dtot(match(it1),3)=corrval 
     
        ! Inversion for dt/t
        !CALL lscov(NWin,dtmat4(:),dtmat(:,1),dtmat(:,2)+1,dtot(1),dtot(2)) ! now replaced by dgels
        ! dtmat(:,1) and dtmat4 can't be overwritten if using OpenMP
        dtmat(:,3)=dtmat(:,3)+UN
        CALL lscov2(NWin,invNWinm1,dtmat4(:),dtmat(:,1),dtmat(:,3),dtot(match(it1),1),dtot(match(it1),2),LWORK)
      ENDDO
  
    ELSE

!!! calculating doublets between every two core and doing inversion

    ! Two nested loop on nstep ! Total number of valid hours
    idx=0
    DO it1=1,nstepm1
  
      ! WindowRef
  
      DO itnwin=1,NWin
        CurrentFunction => dcorr(loc_idx(1,itnwin):loc_idx(2,itnwin),it1)
        CALL applyTKW_noinit_nopad(CurrentFunction,LWinFs,tkw,WindowRefTot(:,itnwin))
      ENDDO

      DO it2=it1+1,nstep
        idx=idx+1
        ! WindowCurrent
        ! compute doublet
  
        corrval=ZERO
        DO itnwin=1,NWin
          CurrentFunction => dcorr(loc_idx(1,itnwin):loc_idx(2,itnwin),it2)
          CALL applyTKW_noinit_nopad(CurrentFunction,LWinFs,tkw,WindowCurrent)
          CALL measureDt(npc,LWinFs,nf,LWORK, &
                         FreqVec,phase(:,itnwin,it1),phase(:,itnwin,it2), &
                         axall(itnwin,it1),axall(itnwin,it2),sxxall(itnwin,it1),sxxall(itnwin,it2), &
                         WindowRefTot(:,itnwin),WindowCurrent, &
                         dtmat(itnwin,1),dtmat(itnwin,2),dtmat(itnwin,3)) ! dt, dterr, corrval
          corrval=corrval+dtmat(itnwin,3)
        ENDDO
  
        if ( ln_writedtmat ) THEN  
          dtmat_save(:,1,idx) = dtmat(:,1)
          dtmat_save(:,2,idx) = dtmat(:,2)
          dtmat_save(:,3,idx) = dtmat(:,3)
        endif

        corrval=corrval*invNWin
        dtot(match(idx),3)=corrval 
     
        ! Inversion for dt/t
        !CALL lscov(NWin,dtmat4(:),dtmat(:,1),dtmat(:,2)+1,dtot(1),dtot(2)) ! now replaced by dgels
        ! dtmat(:,1) and dtmat4 can't be overwritten if using OpenMP
        dtmat(:,3)=dtmat(:,3)+UN
        CALL lscov2(NWin,invNWinm1,dtmat4(:),dtmat(:,1),dtmat(:,3),dtot(match(idx),1),dtot(match(idx),2),LWORK)
      ENDDO
  
    ENDDO

    ENDIF
!!!!!!!! end option

    DEALLOCATE(dtmat,dtmat4,loc_idx,WindowRefTot,WindowCurrent)

    if ( ln_writedtmat ) THEN  
      if ( nn_dt_ref .GE. 0 ) then
        ALLOCATE(rdtmat_save(NWin,3,nstep))
        outdims3=(/NWin,3,nstep/)
      else
        ALLOCATE(rdtmat_save(NWin,3,nstep*(nstep-1)/2))
        outdims3=(/NWin,3,nstep*(nstep-1)/2/)
      endif
      rdtmat_save(:,:,:)=REAL(dtmat_save(:,:,:))
      CALL h5ltmake_dataset_float_f(outfile_id, '/dtmat_save', 3, outdims3, rdtmat_save(:,:,:), error)
      if (error /= 0) call exit(error)
      CALL h5fclose_f(outfile_id, error)
      if (error /= 0) call exit(error)
      DEALLOCATE(dtmat_save,rdtmat_save)
    endif

    ! Create output file and write dtot
    CALL h5fcreate_f('dtot_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
    if (error /= 0) call exit(error)
    if ( nn_dt_ref .GE. 0 ) then
      ALLOCATE(rdtot(ngit,3))
    else
      ALLOCATE(rdtot(npit,3))
    endif
    rdtot(:,:)=REAL(dtot(:,:))
    CALL h5ltmake_dataset_float_f(outfile_id, '/dtot', 2, outdims, rdtot(:,:), error)
    if (error /= 0) call exit(error)
    DEALLOCATE(rdtot)
    CALL h5fclose_f(outfile_id, error)
    if (error /= 0) call exit(error)
  
    CALL date_and_time(VALUES=values)
    elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
    PRINT '("Total elapsed time to compute all doublets : ",f20.6," s")',elps
  
    if ( ln_inversion ) then
  
  ! Invert doublet measurements for all correlation pairs to retrieve dt/t final values
  ! Inversion using least squares with data and model covariance matrices as in Tarantola and Valette :
  ! model = (Gt * Cd-1 .* G + Cm-1)-1 * Gt .* Cd-1 * data
  ! Cmpost = (Gt * Cd-1 .* G + Cm-1)-1 [N,N]
  ! - data [N(N-1)/2]
  ! - G [N(N-1)/2,N] matrice de transfert (creuse)
  ! - Cd [N(N-1)/2] is a covariance matrix (vector) built from error measurements in the doublet analysis (ResDoublets.pairs(ii).dtot(2))
  ! - Cm [N,N] is a covariance matrix on model params built from distance between model params. It represents the temporal smoothing of the final dt/t curve
  ! - Cm(i,j)=exp( -(dist(i,j)) / (2*Lcorr) ) avec Lcorr en jours represente la longueur de correlation : matrice bande, de Toeplitz, symétrique (Cm-1 aussi)
  ! - Gt * Cd-1 .* G : matrice symétrique, somme des ligne=0, somme des col=0
  ! Pour chaque paire de traces :
  ! - dtot = model [N]
  ! - apostError = diag(Cmpost) [N]
  ! - resolution = diag( ( (Gt * Cd-1 * G + Cm-1)-1 * Gt .* Cd-1 ) * Gmat ) [N]
  ! - misfit = rms(G*dtot-data) [1]

      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)
  
      ! Cd^(-1) : on inverse dtot(:,2) : error sur mesure de dtot sur toutes les paires
      DO ii=1,npit
        IF (dtot(ii,2)==ZERO) THEN
#ifdef lk_float
          dtot(ii,2)=0.01
#else
          dtot(ii,2)=0.01d0
#endif
        ELSE
          dtot(ii,2)=UN/dtot(ii,2) ! si on n'ecrit pas dtot sur disque, on aurait pu inverser avant et init dtot(2,:)=0.01 avant calcul des doublets
        ENDIF
      ENDDO
  
      ! Read nn_pondcoef * Cm^(-1) from the input file Cminv.h5
      ALLOCATE(MatCmP(ngit*(ngit+1)/2))
      write(cN,'(I0.8)') ngit
      write(cL,'(I0.8)') nn_Lcorr
      write(cP,'(I0.8)') nn_pondcoef
      CALL h5fopen_f("Cminv_N"//cN//"_L"//cL//"_P"//cP//".h5", H5F_ACC_RDONLY_F, infile_id(1), error)
      if (error /= 0) call exit(error)
      indims = (/ (ngit*(ngit+1)/2) /)
#ifdef lk_float
      CALL h5ltread_dataset_float_f(infile_id(1), "/Cminv_packed_storage", MatCmP(:), indims, error)
#else
      CALL h5ltread_dataset_double_f(infile_id(1), "/Cminv_packed_storage", MatCmP(:), indims, error)
#endif
      if (error /= 0) call exit(error)
      CALL h5fclose_f(infile_id(1), error)
      if (error /= 0) call exit(error)

      ! Compute AP = inv(Cmpost) = Gt * Cd^(-1) .* G + Cm^(-1)
      ! Or Cmpost est symetrique donc on ne stocke qu'un triangle avec sa diagonale (N(N+1)/2)
      ALLOCATE(AP(ngit*(ngit+1)/2))
      ! Create the right hand side dtoti = Gt .* Cd^(-1) * data [N] and ...
      ! ... Resolve model = ( Gt * Cd^(-1) .* G + Cm^(-1) )^(-1) * ( Gt .* Cd^(-1) * data )
      !              dvov  =                  AP                  *         dtoti
      ALLOCATE(dvov(ngit))
      CALL compute_dvov(ngit,npit,dtot(:,1),dtot(:,2),MatCmP,AP,dvov)
      DEALLOCATE(MatCmP)
      CALL h5fcreate_f('dvov_'//TRIM(cn_file1)//'_'//TRIM(cn_file2)//'.h5', H5F_ACC_TRUNC_F, outfile_id, error)
      if (error /= 0) call exit(error)
      outdims1=(/ngit/)
      ALLOCATE(rdvov(ngit))
      rdvov(:)=REAL(dvov(:))
      CALL h5ltmake_dataset_float_f(outfile_id, '/dtot', 1, outdims1, rdvov(:), error)
      if (error /= 0) call exit(error)
      DEALLOCATE(rdvov)
  
      ! Calcul de resolution = diag( ( (Gt * Cd-1 * G + Cm-1)-1 * Gt .* Cd-1 ) * Gmat ) [N]
  ! pas compris cette version du calcul de resolution ?
      ALLOCATE(resolution(ngit))
      CALL compute_resolution(ngit,npit,dtot(:,2),AP,resolution)
      CALL h5ltmake_dataset_float_f(outfile_id, '/resolution', 1, outdims1, resolution(:), error)
      if (error /= 0) call exit(error)
      DEALLOCATE(resolution)
  
      ! Computes apostError = diag(Cmpost) [N]
      ALLOCATE(ApostError(ngit))
      CALL compute_ApostError(ngit,AP,ApostError)
      DEALLOCATE(AP)
      CALL h5ltmake_dataset_float_f(outfile_id, '/ApostError', 1, outdims1, ApostError(:), error)
      if (error /= 0) call exit(error)
      DEALLOCATE(ApostError)
  
      ! calcul misfit = rms(G*dtot-data)
      CALL compute_misfit(npit,ngit,dvov,dtot(:,1),misfit)
      outdims1=(/1/)
      CALL h5ltmake_dataset_float_f(outfile_id, '/misfit', 1, outdims1,misfit, error)
      if (error /= 0) call exit(error)
  
      CALL h5fclose_f(outfile_id, error)
      if (error /= 0) call exit(error)

      CALL date_and_time(VALUES=values)
      elps=values(5)*3600+values(6)*60+values(7)+0.001*values(8)-elps
      PRINT '("Total elapsed time to invert doublets : ",f20.6," s")',elps

    endif

  endif

  CALL h5close_f(error)
  if (error /= 0) call exit(error)

END PROGRAM corr_doublet_inversion_bypair
