!#---------------------------------
!$Id: corrmodule.f90 240 2017-09-20 12:00:55Z lecointre $
!#---------------------------------

MODULE corrmodule

  USE, intrinsic :: iso_c_binding
  USE HDF5
#include "types.h"
  USE constants

  IMPLICIT NONE

include 'fftw3.f03'

  CONTAINS

!------------------------------------------------------------------------------
  SUBROUTINE create_or_open_group(parent_id,gr_name,gr_id)
    USE HDF5 ! HDF5 module
    IMPLICIT NONE
    INTEGER(HID_T), INTENT(in)     :: parent_id
    CHARACTER(LEN=*), INTENT(in)   :: gr_name
    INTEGER(HID_T), INTENT(out)    :: gr_id
    LOGICAL                        :: link_exists
    INTEGER                        :: error
    ! Output file : create group if necessary, or simply open it
    CALL h5lexists_f(parent_id, gr_name, link_exists, error, H5P_DEFAULT_F)
    if (error /= 0) call exit(error)
    IF ( link_exists ) THEN
      CALL h5gopen_f(parent_id, gr_name, gr_id, error)
    ELSE
      CALL h5gcreate_f(parent_id, gr_name, gr_id, error)
    ENDIF
    if (error /= 0) call exit(error)
  END SUBROUTINE create_or_open_group
  
!------------------------------------------------------------------------------
#ifdef lk_2Dinput

  SUBROUTINE read_one_trace(file_id,dsetname,offset,count,nb,buf)
    USE HDF5
    IMPLICIT NONE
    INTEGER(HID_T), INTENT(in)     :: file_id
    CHARACTER(LEN=*), INTENT(in)   :: dsetname
    INTEGER, INTENT(in)            :: nb
    INTEGER(HSIZE_T), DIMENSION(2), INTENT(in) :: offset
    INTEGER(HSIZE_T), DIMENSION(2), INTENT(in) :: count
#if defined lk_1bit
    INTEGER, DIMENSION(nb), INTENT(out) :: buf
#else
    REAL(KIND=4), DIMENSION(nb), INTENT(out) :: buf
#endif
    INTEGER(HID_T) :: dset_id,space_id,memspace_id
    INTEGER                        :: error
    INTEGER(HSIZE_T), DIMENSION(1) :: memcount
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memoffset = (/0/) 
    INTEGER(HSIZE_T), DIMENSION(1) :: indims
    memcount=(/nb/)
    indims = (/nb/)
    CALL h5dopen_f(file_id, dsetname, dset_id, error)
    if (error /= 0) call exit(error)
    CALL h5dget_space_f(dset_id, space_id, error)
    if (error /= 0) call exit(error)
    CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
    if (error /= 0) call exit(error)
    CALL h5screate_simple_f(1, memcount, memspace_id, error)
    if (error /= 0) call exit(error)
    CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memoffset, memcount, error)
    if (error /= 0) call exit(error)
#if defined lk_1bit
    buf(:)=0
    CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, buf, indims, error, memspace_id, space_id)
    if (error /= 0) call exit(error)
#else
    buf(:)=0.0
    CALL H5dread_f(dset_id, H5T_NATIVE_REAL, buf, indims, error, memspace_id, space_id)
    if (error /= 0) call exit(error)
#endif
    CALL h5sclose_f(space_id, error)
    if (error /= 0) call exit(error)
    CALL h5sclose_f(memspace_id, error)
    if (error /= 0) call exit(error)
    CALL h5dclose_f(dset_id, error)
    if (error /= 0) call exit(error)
  END SUBROUTINE read_one_trace

#else

  SUBROUTINE read_one_trace(file_id,dsetname,offset,count,nb,buf)
    USE HDF5
    IMPLICIT NONE
    INTEGER(HID_T), INTENT(in)     :: file_id
    CHARACTER(LEN=*), INTENT(in)   :: dsetname
    INTEGER, INTENT(in)            :: nb
    INTEGER(HSIZE_T), DIMENSION(1), INTENT(in) :: offset
    INTEGER(HSIZE_T), DIMENSION(1), INTENT(in) :: count
#if defined lk_1bit
    INTEGER, DIMENSION(nb), INTENT(out) :: buf
#else
    REAL(KIND=4), DIMENSION(nb), INTENT(out) :: buf
#endif
    INTEGER(HID_T) :: dset_id,space_id,memspace_id
    INTEGER                        :: error
    INTEGER(HSIZE_T), DIMENSION(1) :: memcount
    INTEGER(HSIZE_T), DIMENSION(1), PARAMETER :: memoffset = (/0/) 
    INTEGER(HSIZE_T), DIMENSION(1) :: indims
    memcount=(/nb/)
    indims = (/nb/)
    CALL h5dopen_f(file_id, dsetname, dset_id, error)
    if (error /= 0) call exit(error)
    CALL h5dget_space_f(dset_id, space_id, error)
    if (error /= 0) call exit(error)
    CALL h5sselect_hyperslab_f (space_id, H5S_SELECT_SET_F, offset, count, error)
    if (error /= 0) call exit(error)
    CALL h5screate_simple_f(1, memcount, memspace_id, error)
    if (error /= 0) call exit(error)
    CALL h5sselect_hyperslab_f(memspace_id, H5S_SELECT_SET_F, memoffset, memcount, error)
    if (error /= 0) call exit(error)
#if defined lk_1bit
    buf(:)=0
    CALL H5dread_f(dset_id, H5T_NATIVE_INTEGER, buf, indims, error, memspace_id, space_id)
    if (error /= 0) call exit(error)
#else
    buf(:)=0.0
    CALL H5dread_f(dset_id, H5T_NATIVE_REAL, buf, indims, error, memspace_id, space_id)
    if (error /= 0) call exit(error)
#endif
    CALL h5sclose_f(space_id, error)
    if (error /= 0) call exit(error)
    CALL h5sclose_f(memspace_id, error)
    if (error /= 0) call exit(error)
    CALL h5dclose_f(dset_id, error)
    if (error /= 0) call exit(error)
  END SUBROUTINE read_one_trace

#endif
  
!------------------------------------------------------------------------------
  FUNCTION LEAP (YEAR) RESULT (LEAPFLAG)
  !  Input:
  !     year  -  Gregorian year (integer)
  !  Output:
  !     Function return value = .true. if year is a leap year, and .false. otherwise.
    IMPLICIT NONE
    INTEGER :: YEAR
    LOGICAL :: LEAPFLAG
    LEAPFLAG = .FALSE.
    IF (MOD(YEAR,4) .EQ. 0)   LEAPFLAG = .TRUE.
    IF (MOD(YEAR,100) .EQ. 0) LEAPFLAG = .FALSE.
    IF (MOD(YEAR,400) .EQ. 0) LEAPFLAG = .TRUE.
    RETURN
  END FUNCTION LEAP
  
!------------------------------------------------------------------------------
  function FIND_NJDAY(NDATBEG,NDATEND) result(N)
    IMPLICIT NONE
    integer, intent(in) :: NDATBEG,NDATEND
    INTEGER :: N
    INTEGER :: jyear,jday,iadd
    N = 0
    ! if year 1 = year end
    IF ( NDATBEG/1000 .EQ. NDATEND/1000 ) THEN
      jyear = NDATBEG/1000
      DO jday = NDATBEG-1000*jyear,NDATEND-1000*jyear
        N = N+1
      ENDDO
    ! if year 1 < year end
    ELSE
      ! first year
      jyear = NDATBEG/1000
      iadd=0
      IF (LEAP(jyear)) iadd=1
      DO jday = NDATBEG-1000*jyear,365+iadd
        N = N+1
      ENDDO
      ! year 2 - (n-1)
      DO jyear = 1+NDATBEG/1000, -1+NDATEND/1000
        iadd=0
        IF (LEAP(jyear)) iadd=1
        DO jday = 1,365+iadd
          N = N+1
        ENDDO
      ENDDO
      ! year n
      jyear=NDATEND/1000
      DO jday = 1,NDATEND-1000*(jyear)
        N = N+1
      ENDDO
    ENDIF
  END FUNCTION FIND_NJDAY
  
!------------------------------------------------------------------------------
  FUNCTION MAKE_DAYTIME_VECTOR(N,NDATBEG,NDATEND) result(IARR)
    IMPLICIT NONE
    integer, intent(in) :: N,NDATBEG,NDATEND
    INTEGER, DIMENSION(2,N) :: IARR
    INTEGER :: jn,jyear,jday,iadd
    jn = 0
    ! if year 1 = year end
    IF ( NDATBEG/1000 .EQ. NDATEND/1000 ) THEN
      jyear = NDATBEG/1000
      DO jday = NDATBEG-1000*jyear,NDATEND-1000*jyear
        jn = jn+1
        IARR(1,jn) = jyear
        IARR(2,jn) = jday
      ENDDO
    ! if year 1 < year end
    ELSE
      ! first year
      jyear = NDATBEG/1000
      iadd=0
      IF (LEAP(jyear)) iadd=1
      DO jday = NDATBEG-1000*jyear,365+iadd
        jn = jn+1
        IARR(1,jn) = jyear
        IARR(2,jn) = jday
      ENDDO
      ! year 2 - (n-1)
      DO jyear = 1+NDATBEG/1000, -1+NDATEND/1000
        iadd=0
        IF (LEAP(jyear)) iadd=1
        DO jday = 1,365+iadd
          jn = jn+1
          IARR(1,jn) = jyear
          IARR(2,jn) = jday
        ENDDO
      ENDDO
      ! year n
      jyear=NDATEND/1000
      DO jday = 1,NDATEND-1000*(jyear)
        jn = jn+1
        IARR(1,jn) = jyear
        IARR(2,jn) = jday
      ENDDO
    ENDIF
  END FUNCTION MAKE_DAYTIME_VECTOR

!------------------------------------------------------------------------------
  FUNCTION FIND_VALID_NJDAY(N,IARR,FID) RESULT(NV)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: N
    INTEGER, DIMENSION(2,N), INTENT(IN) :: IARR
    INTEGER(HID_T), DIMENSION(2), INTENT(IN) :: FID
    INTEGER :: NV
    INTEGER :: jt,error
    CHARACTER(LEN=3) :: cjd
    CHARACTER(LEN=4) :: cyear
    LOGICAL, DIMENSION(2) :: linkyear_exists,linkjday_exists
    NV = 0
    DO jt = 1,N
      write(cyear,'(I0.4)') IARR(1,jt)
      CALL h5lexists_f(FID(1), "/"//TRIM(cyear), linkyear_exists(1), error, H5P_DEFAULT_F)
      if (error /= 0) call exit(error)
      CALL h5lexists_f(FID(2), "/"//TRIM(cyear), linkyear_exists(2), error, H5P_DEFAULT_F)
      if (error /= 0) call exit(error)
      IF ( linkyear_exists(1) .AND. linkyear_exists(2) ) THEN
        write(cjd,'(I0.3)') IARR(2,jt)
        CALL h5lexists_f(FID(1), "/"//TRIM(cyear)//"/"//TRIM(cjd), linkjday_exists(1), error, H5P_DEFAULT_F)
        if (error /= 0) call exit(error)
        CALL h5lexists_f(FID(2), "/"//TRIM(cyear)//"/"//TRIM(cjd), linkjday_exists(2), error, H5P_DEFAULT_F)
        if (error /= 0) call exit(error)
        IF ( linkjday_exists(1) .AND. linkjday_exists(2) ) NV=NV+1
      ENDIF
    ENDDO
  END FUNCTION FIND_VALID_NJDAY

!------------------------------------------------------------------------------
  FUNCTION IS_VALID_JDAY(N,IARR,FID) RESULT(IARRV)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: N
    INTEGER, DIMENSION(2,N), INTENT(IN) :: IARR
    INTEGER(HID_T), DIMENSION(2), INTENT(IN) :: FID
    INTEGER, DIMENSION(N) :: IARRV
    INTEGER :: jt,error
    CHARACTER(LEN=3) :: cjd
    CHARACTER(LEN=4) :: cyear
    LOGICAL, DIMENSION(2) :: linkyear_exists,linkjday_exists
    IARRV(:) = 0
    DO jt = 1,N
      write(cyear,'(I0.4)') IARR(1,jt)
      CALL h5lexists_f(FID(1), "/"//TRIM(cyear), linkyear_exists(1), error, H5P_DEFAULT_F)
      if (error /= 0) call exit(error)
      CALL h5lexists_f(FID(2), "/"//TRIM(cyear), linkyear_exists(2), error, H5P_DEFAULT_F)
      if (error /= 0) call exit(error)
      IF ( linkyear_exists(1) .AND. linkyear_exists(2) ) THEN
        write(cjd,'(I0.3)') IARR(2,jt)
        CALL h5lexists_f(FID(1), "/"//TRIM(cyear)//"/"//TRIM(cjd), linkjday_exists(1), error, H5P_DEFAULT_F)
        if (error /= 0) call exit(error)
        CALL h5lexists_f(FID(2), "/"//TRIM(cyear)//"/"//TRIM(cjd), linkjday_exists(2), error, H5P_DEFAULT_F)
        if (error /= 0) call exit(error)
        IF ( linkjday_exists(1) .AND. linkjday_exists(2) ) IARRV(jt)=1
      ENDIF
    ENDDO
  END FUNCTION IS_VALID_JDAY
  
!------------------------------------------------------------------------------
#ifdef lk_2Dinput
  SUBROUTINE COMPUTE_FFTPROD(nph,js,maxlag,npad,FID,cyear,cjd,chour,plan_forward,fftprod,ln_naninf)
  ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nph,js,maxlag,npad
    INTEGER(HID_T), DIMENSION(2), INTENT(IN) :: FID
    CHARACTER(LEN=2), INTENT(IN) :: chour
    CHARACTER(LEN=3), INTENT(IN) :: cjd
    CHARACTER(LEN=4), INTENT(IN) :: cyear
    INTEGER(KIND=8), INTENT(IN) :: plan_forward 
    COMPLEX(MYKINDCPLX), DIMENSION(npad/2+1), INTENT(OUT) :: fftprod
    INTEGER(HSIZE_T), DIMENSION(2) :: startin    ! The starting point to read 20min input dataset from input file 
    INTEGER(HSIZE_T), DIMENSION(2) :: countin    ! Into 2Ddataset from HDF5 file: nb of points when read 20min input data from input file : (/1,24000/)
#if defined lk_1bit
    INTEGER, DIMENSION(nph) :: ntr               ! 1bit station traces
#else
    REAL(KIND=4), DIMENSION(nph) :: ntr
#endif
    REAL(MYKIND), DIMENSION(npad) :: dinpad      ! normalized 1bit station traces, padded with zeros before or after
    COMPLEX(MYKINDCPLX), DIMENSION(npad/2+1) :: yout,yout_conjg  ! FFT+1
    LOGICAL, INTENT(OUT) :: ln_naninf
    countin = (/nph,1/)
    startin=(/0,js-1/)
    CALL read_one_trace(FID(1), "/"//TRIM(cyear)//"/"//TRIM(cjd)//"/"//TRIM(chour),startin,countin,nph,ntr)    
    ! Compute the FFT forward first when padding with zero after, and take the conjugate
    dinpad(nph+1:npad)=ZERO
#ifdef lk_float
    dinpad(1:nph) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
    CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
    dinpad(1:nph) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
    CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif
    ! Save the output of FFT+1 in memory 
    yout_conjg(:) = conjg(yout(:))
    CALL read_one_trace(FID(2), "/"//TRIM(cyear)//"/"//TRIM(cjd)//"/"//TRIM(chour),startin,countin,nph,ntr)
    ! Compute again the FFT forward when padding with zero before
    dinpad(1:maxlag)=ZERO
#ifdef lk_float
    dinpad(maxlag+1:npad) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
    CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
    dinpad(maxlag+1:npad) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
    CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif
    fftprod(:) = yout_conjg(:)*yout(:)
    ! checking for nan or inf value
!The simple way without using the ieee_arithmatic is to do the following.
!Infinity: Define your variable infinity = HUGE(dbl_prec_var) (or, if you have it, a quad precision variable). Then you can simply check to see if your variable is infinity by if(my_var > infinity).
!NAN: This is even easier. By definition, NAN is not equal to anything, even itself. Simply compare the variable to itself: if(my_var /= my_var).
    if ( ( DBLE(fftprod(1)) > huge(1.d0) ) .OR. ( fftprod(1) /= fftprod(1) ) ) then
      ln_naninf = .true.
    else
      ln_naninf = .false.
    endif

  END SUBROUTINE COMPUTE_FFTPROD
#else
  SUBROUTINE COMPUTE_FFTPROD(nph,js,maxlag,npad,FID,cyear,cjd,plan_forward,fftprod,ln_naninf)
  ! Compute conjg(FFT(tr1)) * FFT(tr2) with appropriate zero padding 
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: nph,js,maxlag,npad
    INTEGER(HID_T), DIMENSION(2), INTENT(IN) :: FID
    CHARACTER(LEN=3), INTENT(IN) :: cjd
    CHARACTER(LEN=4), INTENT(IN) :: cyear
    INTEGER(KIND=8), INTENT(IN) :: plan_forward 
    COMPLEX(MYKINDCPLX), DIMENSION(npad/2+1), INTENT(OUT) :: fftprod
    INTEGER(HSIZE_T), DIMENSION(1) :: startin    ! The starting point to read 20min input dataset from input file 
    INTEGER(HSIZE_T), DIMENSION(1) :: countin    ! Into 2Ddataset from HDF5 file: nb of points when read 20min input data from input file : (/1,24000/)
#if defined lk_1bit
    INTEGER, DIMENSION(nph) :: ntr               ! 1bit station traces
#else
    REAL(KIND=4), DIMENSION(nph) :: ntr
#endif
    REAL(MYKIND), DIMENSION(npad) :: dinpad      ! normalized 1bit station traces, padded with zeros before or after
    COMPLEX(MYKINDCPLX), DIMENSION(npad/2+1) :: yout,yout_conjg  ! FFT+1
    LOGICAL, INTENT(OUT) :: ln_naninf
    countin = (/nph/)
    startin=(/js/)
    CALL read_one_trace(FID(1), "/"//TRIM(cyear)//"/"//TRIM(cjd),startin,countin,nph,ntr)    
    ! Compute the FFT forward first when padding with zero after, and take the conjugate
    dinpad(nph+1:npad)=ZERO
#ifdef lk_float
    dinpad(1:nph) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
    CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
    dinpad(1:nph) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
    CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif
    ! Save the output of FFT+1 in memory 
    yout_conjg(:) = conjg(yout(:))
    CALL read_one_trace(FID(2), "/"//TRIM(cyear)//"/"//TRIM(cjd),startin,countin,nph,ntr)
    ! Compute again the FFT forward when padding with zero before
    dinpad(1:maxlag)=ZERO
#ifdef lk_float
    dinpad(maxlag+1:npad) = REAL(ntr(:))/SQRT(SUM((REAL(ntr(:))**2)))
    CALL sfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#else
    dinpad(maxlag+1:npad) = DBLE(ntr(:))/SQRT(SUM((DBLE(ntr(:))**2)))
    CALL dfftw_execute_dft_r2c(plan_forward, dinpad, yout)
#endif
    fftprod(:) = yout_conjg(:)*yout(:)
    ! checking for nan or inf value
!The simple way without using the ieee_arithmatic is to do the following.
!Infinity: Define your variable infinity = HUGE(dbl_prec_var) (or, if you have it, a quad precision variable). Then you can simply check to see if your variable is infinity by if(my_var > infinity).
!NAN: This is even easier. By definition, NAN is not equal to anything, even itself. Simply compare the variable to itself: if(my_var /= my_var).
    if ( ( DBLE(fftprod(1)) > huge(1.d0) ) .OR. ( fftprod(1) /= fftprod(1) ) ) then
      ln_naninf = .true.
    else
      ln_naninf = .false.
    endif
  END SUBROUTINE COMPUTE_FFTPROD
#endif  

SUBROUTINE compute_fftw_plan(plan, npad, plantype)
  ! Create the plans for FFT forward or FFT backward
  IMPLICIT NONE
  INTEGER(KIND=8), INTENT(OUT) :: plan
  INTEGER, INTENT(IN) :: npad, plantype
  INTEGER, PARAMETER :: rank = 1
  INTEGER, DIMENSION(rank) :: n_array
  COMPLEX(MYKINDCPLX), DIMENSION(npad/2+1) :: yout
  REAL(MYKIND), DIMENSION(npad) :: din
  n_array(1) = npad
  if (plantype == 1) THEN ! forward
    din(:)=ZERO
#ifdef lk_float
    CALL sfftw_plan_dft_r2c(plan, rank, n_array, &
                            din, yout, FFTW_ESTIMATE)
#else
    CALL dfftw_plan_dft_r2c(plan, rank, n_array, &
                            din, yout, FFTW_ESTIMATE)
#endif
  else ! backward
    yout(:)=(ZERO,ZERO)
#ifdef lk_float
    CALL sfftw_plan_dft_c2r(plan, rank, n_array, &
                            yout, din, FFTW_ESTIMATE)
#else
    CALL dfftw_plan_dft_c2r(plan, rank, n_array, &
                            yout, din, FFTW_ESTIMATE)
#endif
  endif
END SUBROUTINE compute_fftw_plan

END MODULE corrmodule
