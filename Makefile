# module load intel-devel/13 fftw/3.3.4-precise_intel-13.0.1 hdf5/1.8.14_intel-13.0.1 szip/2.1_gcc-4.6.2 zlib/1.2.7_gcc-4.6.2

# CPP keys and Compiler flags

CPPK      =  #-Dlk_2Dinput # -Dlk_float # -Dlk_1bit # -Dlk_float   # attention ne pas faire les calculs en float pour l'inversion !!! 
FFLAGS    = -cpp $(CPPK) -O2 #  -warn all -check all -O0  -fno-alias -fno-fnalias  -g -debug -traceback -check all  -implicitnone -warn all  -fpe0 -fp-stack-check -ftrapuv -heap-arrays -gen-interface -warn interface   # -02 -warn all -check all -O2 # -g # -O2 # -warn all -check all # -static-intel # -openmp # -pg -g # -check all
#FFLAGS    = -cpp $(CPPK) -O2 # -static-intel # -openmp # -pg -g # -check all

# ------- Machine specific usage ----------------------

FC        = h5pfc

#LIBLA = /applis/ciment/v2/x86_64/DEFAULT/gcc_4.6.2/lib/libgfortran.so.3 -L/applis/ciment/v2/stow/x86_64/gcc_4.6.2/lapack_3.3.0/ -llapack -lblas
#LIBLA = -llapack -lblas
LIBLA = -mkl=sequential

LIBFFTW = -L${fftw_DIR}/lib -lfftw3 
#LIBFFTW = -L${fftw_DIR}/lib -lfftw3f

# ------ No machine-specific paths/variables after this  -----

FSOURCE = constants.o corrmodule.o doubletmodule.o inversionmodule.o corr_doublet_inversion_bypair compute_Cminv

FLAGS   = -c  

all:    $(FSOURCE)

constants.o: constants.f90
	$(FC) $(FFLAGS) $(FLAGS) -o $@ $^ 

corrmodule.o: corrmodule.f90
	$(FC) $(FFLAGS) $(FLAGS) -o $@ $^ 

doubletmodule.o: doubletmodule.f90
	$(FC) $(FFLAGS) $(FLAGS) -o $@ $^ 

inversionmodule.o: inversionmodule.f90
	$(FC) $(FFLAGS) $(FLAGS) -o $@ $^ 

corr_doublet_inversion_bypair: constants.o corrmodule.o doubletmodule.o inversionmodule.o corr_doublet_inversion_bypair.f90
	$(FC) $(FFLAGS) -o $@ $^ $(LIBFFTW) $(LIBLA) 

compute_Cminv: corrmodule.o compute_Cminv.f90
	$(FC) $(FFLAGS) -o $@ $^ $(LIBFFTW) $(LIBLA)

clean:
	rm -f $(FSOURCE) *.mod 

.f90.o: $(FSOURCE)
	$(FC) $(FFLAGS) $(FLAGS) $?
.SUFFIXES:.o.c.f90
